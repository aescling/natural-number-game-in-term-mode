import «NaturalNumberGameInTermMode».World06AdvancedAddition

/-
to say that `a` is less than or equal to `b`, typically expurressed in Lean
with the notation `a ≤ b`, is definable on the natural numbers as "there exists
a number `c` such that `b = add a c`", written as follows:
-/
def Leq (a b : MyNat.Nat) :=
  ∃ (c : MyNat.Nat), b = MyNat.add a c
/-
`∃` is a special case of a dependent pair where both members of the pair live
in `Prop`, the lowest-level universe of the type hierarchy. (higher levels of
types are able to contain lower levels: we do not want to allow `Prop : Prop`,
but it is useful to have types of types such that `Prop : Type`, and
`Type : Type 1`, and so on and so furth.)

dependent pairs are pairs of the type `(a : α), β a`─hat is, the type of the
second elemet depends on the *value* of the furst.

`∃`, by the way, is notation fur `Exists`

`#print Exists`
`inductive Exists.{u} : {α : Sort u} → (α → Prop) → Prop`
`number of parameters: 2`
`constructors:`
`Exists.intro : ∀ {α : Sort u} {p : α → Prop} (w : α), p w → Exists p`

the eliminator on the `Exists` type is a convoluted method of extracting the
furst and second parts of the pair, by making them available in lambda
bindings:

`check Exists.rec`
`Exists.rec.{u} {α : Sort u} {p : α → Prop}`
  `{motive : Exists p → Prop}`
  `(intro : ∀ (w : α) (h : p w), motive ⋯)`
  `(t : Exists p) : motive t`

fur example: `Exists.rec (intro := λ w _ ↦ w)` is the equivalet of `car`, or in
Lean syntax, the `.1` destructor of a pair; `Exists.rec (intro := λ _ h ↦ h)`
is the equivalent of `cdr,` or in Lean syntax, the `.2` descructor of a pair.

it is generally purrefurable to use `Exists.elim`, an extremely thin wrapper
around `.rec` with a much more obvious typing. like `.recOn`, the furst
explicit arugment is an `Exists`, so you can `.elim` on a value of the type.
-/

namespace WorldLeqLevel01
  open MyNat

  variable (x : MyNat.Nat)

  example : Leq x x :=
    Exists.intro
      (w := zero)
      (h :=
        -- goal: `x = add x zero`
        rfl)
end WorldLeqLevel01

/-
Lean has generic syntax fur introducing a member of a type that has only one
constructor, in `⟨⟩`; arguments to the contructor are purrovided as a comma-
sepurrated list to the syntax. This is only allowed when Lean can infur the
type being introduced, and is not always clearer than an explicit invocation of
the constructor.
-/
namespace WorldLeqLevel02
  open MyNat

  variable (x : MyNat.Nat)

  theorem zero_le : Leq zero x :=
    ⟨
      x,
      -- goal: `x = add zero x`
      flip Eq.mp (Eq.refl x) $ congrArg
        _
        $ Eq.symm $ zero_add _
    ⟩
end WorldLeqLevel02

namespace MyNat
  def zero_le := WorldLeqLevel02.zero_le
end MyNat

namespace WorldLeqLevel03
  open MyNat

  variable (x : MyNat.Nat)

  theorem le_succ_self : Leq x (succ x) :=
    ⟨
      one,
      -- goal: `succ x = add x one`
      rfl
    ⟩
end WorldLeqLevel03

namespace MyNat
  def le_succ_self := WorldLeqLevel03.le_succ_self
end MyNat

/-
this level requires us to destructure a `Leq`. in purractice, using the actual
eliminator, it is not terribly cumbersome, but we will later show nicer
syntacic sugar fur this.
-/
namespace WorldLeqLevel04
  open MyNat

  variable (x y z : MyNat.Nat)
  variable (hxy : Leq x y)
  variable (hyz : Leq y z)

  theorem le_trans : Leq x z :=
    hxy.recOn
      (intro := λ n₁ (hyxn₁ : y = add x n₁) ↦
        hyz.recOn
          (intro := λ n₂ (hzyn₂ : z = add y n₂) ↦
            /-
            goal: `z = add x (add n₁ n₂)`

            `  z = add x (add n₁ n₂)`
            `← z = add (add x n₁) n₂  [by add_assoc]`
            `← z = add y n₂           [by hyxn₁]`
            `←                        [by hzyn₂]`
            -/
            Exists.intro
              (w := add n₁ n₂)
              (h :=
                let h
                  : z = add (add x n₁) n₂
                  := (flip Eq.mp) hzyn₂ $ congrArg
                    ((Eq z) ∘ (flip add $ _))
                    hyxn₁

                (flip Eq.mp) h $ congrArg
                    _
                    $ add_assoc ..)))
end WorldLeqLevel04

namespace MyNat
  def le_trans := WorldLeqLevel04.le_trans
end MyNat

/-
in this level we show using `.elim` instead of `rec{,On}`; in purractice there
is not much of a meaningful diffurence.
-/
namespace WorldLeqLevel05
  open MyNat

  variable (x : MyNat.Nat)
  variable (h : Leq x zero)

  theorem le_zero : x = zero :=
    h.elim
      (λ z (hz : zero = add x z) ↦
        add_right_eq_zero x z $ Eq.symm hz)
end WorldLeqLevel05

namespace MyNat
  def le_zero := WorldLeqLevel05.le_zero
end MyNat

/-
in this level we show that the `⟨⟩` syntax is also used fur destructuring with
`.elim` on a type, introducing bindings to the components of the type without
(explicitly) introducing lambdas into the code. this is nicer to work with when
nested destructuring is required.
-/
namespace WorldLeqLevel06
  open MyNat

  variable (x y : MyNat.Nat)
  variable (hxy : Leq x y)
  variable (hyx : Leq y x)

  theorem le_antisymm : x = y :=
    let ⟨n₁, (hyn₁ : y = add x n₁)⟩ := hxy
    let ⟨n₂, (hxn₂ : x = add y n₂)⟩ := hyx
    /-
    it suffices to purrove `n₂ = zero`: after substitution, `hxn₂` evaluates to
    the goal.

    `      x             = add y n₂          [by hxn₂]`
    `→     x             = add (add x n₁) n₂ [by hyn₁]`
    `→     x             = add x (add n₁ n₂) [by add_assoc]`
    `→ add x (add n₁ n₂) =     x             [by Eq.symm]`
    `→        add n₁ n₂  =     zero          [by add_left_eq_self]`
    `→               n₂  =     zero          [by add_left_eq_zero]`
    -/
    let hn₂
      : n₂ = zero
      := let h
          : x = add (add x n₁) n₂
          := (flip Eq.mp) hxn₂ $ congrArg
            (Eq _ ∘ flip add _)
            $ hyn₁

        let h
          : add x (add n₁ n₂) = x
          := (flip Eq.mp) (Eq.symm h) $ congrArg
            (flip Eq _)
            $ add_assoc ..

        add_left_eq_zero _ _
          ∘ add_right_eq_self _ _
            $ h

    (flip Eq.mp) hxn₂ $ congrArg
      (Eq _ ∘ add _)
      hn₂
end WorldLeqLevel06

namespace MyNat
  def le_antisymm := WorldLeqLevel06.le_antisymm
end MyNat

/-
in this level we introduce the inductive type `Or`:

`#print Or`
`inductive Or : Prop → Prop → Prop`
`number of parameters: 2`
`constructors:`
`Or.inl : ∀ {a b : Prop}, a → a ∨ b`
`Or.inr : ∀ {a b : Prop}, b → a ∨ b`

`Or`'s two constructors capture the intuition that a purroof of `a ∨ b`
("a or b") is just either a purroof of `a` or a purroof of `b`.

read `inl` as "introduce left", `inr` as "introduce right".

`#print Or.rec`
`Or.rec {a b : Prop}`
  `{motive : a ∨ b → Prop}`
  `(inl : ∀ (h : a), motive ⋯)`
  `(inr : ∀ (h : b), motive ⋯)`
  `(t : a ∨ b) : motive t`

`Or`'s eliminator captures the intuition that a purroof of `a ∨ b` could be
either a purroof of `a` or a purroof of `b`, and thus when eliminating it we
need to account fur both cases.

in the three alternative solutions to this level, we show that `casesOn` and
`elim` on `Or` are extremely, extremely thin wrappers over `rec` in purractice.

it is an arguable flaw of a type system like Lean's that the term `a` is not a
member of `a ∨ b`, but rather `Or.inl a`, despite the actual purroofs being
a trivial isomorphism.
-/
namespace WorldLeqLevel07
  open MyNat

  def thirty_seven := MyNat_of 37
  def forty_two := MyNat_of 42

  variable (x y : MyNat.Nat)
  variable (h : x = thirty_seven ∨ y = forty_two)

  example : y = forty_two ∨ x = thirty_seven :=
    h.recOn
      (inl := Or.inr)
      (inr := Or.inl)

  example : y = forty_two ∨ x = thirty_seven :=
    h.casesOn
      (inl := Or.inr)
      (inr := Or.inl)

  example : y = forty_two ∨ x = thirty_seven :=
    h.elim
      (left := Or.inr)
      (right := Or.inl)
end WorldLeqLevel07

namespace WorldLeqLevel08
  open MyNat

  variable (x y : MyNat.Nat)

  theorem le_total : Leq x y ∨ Leq y x :=
    y.recOn
      (zero :=
        -- goal: `Leq x zero ∨ Leq zero x`; the right is true by `zero_le`
        Or.inr $ zero_le _)
      (succ := λ n (h : Leq x n ∨ Leq n x) ↦
        -- goal: Leq x (succ n) ∨ Leq (succ n) x
        h.elim
          (left := λ (h' : Leq x n) ↦
            Or.inl
              $ let ⟨m, (hxm : n = add x m)⟩ := h'
              ⟨
                succ m,
                /-
                goal: `succ n = add x (succ m)`

                `       n = add x m        [by hxm]`
                `→ succ n = succ (add x m) [by ensucc]`
                `→ succ n = add x (succ m) [by add_succ]`
                -/
                flip Eq.mp (ensucc hxm) $ congrArg
                  (Eq _)
                  $ Eq.symm $ add_succ ..
              ⟩)
          (right := λ (h' : Leq n x) ↦
            /-
            instead of immediately destructuring `h'`, of type
            `∃ m, x = add n m`, furst just destruct the `m` that exists,
            and case split on it, so that we can reason about both resultiing
            cases of the dependently typed hypawthesis that `x = add n m`.
            -/
            h'.elim (λ m ↦
              m.casesOn
                (zero := λ (hxz : x = add n zero) ↦
                  Or.inl
                    ⟨
                      one,
                      /-
                      goal : `succ n = add x one`

                      `       n = x         [by Eq.symm hxz (after normalization)]`
                      `→ succ n = succ x    [by ensucc]`
                      `→ succ n = add x one [by succ_eq_add_one]`
                      -/
                      flip Eq.mp (ensucc ∘ Eq.symm $ hxz) $ congrArg
                        (Eq _)
                        $ Eq.symm $ succ_eq_add_one _
                    ⟩)
                (succ := λ m' (hxm : x = add n (succ m')) ↦
                  Or.inr
                    ⟨
                      m',
                      /-
                      goal: `x = add (succ n) m'`

                      `  x = add n (succ m') [by hxm]`
                      `→ x = succ (add n m') [by add_succ]`
                      `→ x = add (succ n) m' [by succ_add]`
                      -/
                      let hEq
                        : x = succ (add n m')
                        := flip Eq.mp hxm $ congrArg
                          (Eq _)
                          $ add_succ ..

                      flip Eq.mp hEq $ congrArg
                        (Eq _)
                        $ Eq.symm $ succ_add ..
                    ⟩))))
end WorldLeqLevel08

namespace MyNat
  def le_total := WorldLeqLevel08.le_total
end MyNat

namespace WorldLeqLevel09
  open MyNat

  variable (x y : MyNat.Nat)
  variable (h : Leq (succ x) (succ y))

  theorem succ_le_succ : Leq x y :=
    let ⟨n, (hyxn : succ y = add (succ x) n)⟩ := h
    ⟨
        n,
        -- goal: `y = add x n`; trivial by `succ_add` and `succ_inj` on `hyxn`
        succ_inj
          $ flip Eq.mp hyxn $ congrArg
            (Eq _)
            $ succ_add ..
    ⟩
end WorldLeqLevel09

namespace MyNat
  def succ_le_succ := WorldLeqLevel09.succ_le_succ
end MyNat

namespace WorldLeqLevel10
  open MyNat
  theorem le_one : (x : MyNat.Nat)  → (hx : Leq x one) →  x = zero ∨ x = one :=
    λ x ↦
      x.casesOn
        (zero := Function.const _ $ Or.inl rfl)
        (succ := λ x' (hx' : Leq (succ x') one) ↦
          Or.inr
            $ let ⟨n, (hn : one = add (succ x') n)⟩ := hx'
            /-
            goal: `succ x' = one`

            `  add (succ x') n = one       [by Eq.symm hn]`
            `→ add (succ x') n = succ zero [by Eq.refl (normalization)] -- or by succ_eq_add_one`
            `→ succ (add x' n) = succ zero [by succ_add]`
            `→       add x' n  = zero      [by succ_inj]`
            `→           x'    = zero      [by add_right_eq_zero]`
            `→      succ x'    = succ zero [by congrArg succ]`
            `→      succ x'    = one       [by Eq.refl (normalization)] -- or by succ_eq_add_one`
            -/
            let hEq
              : add (succ x') n = succ zero
              := Eq.symm hn

            let hEq
              : succ (add x' n) = succ zero
              := flip Eq.mp hEq $ congrArg
                (flip Eq _)
                $ succ_add ..

            let hEq
              : add x' n = zero
              := succ_inj hEq

            let hEq
              : x' = zero
              := add_right_eq_zero _ _ hEq

            congrArg
              succ
              hEq)
end WorldLeqLevel10

namespace MyNat
  def le_one := WorldLeqLevel10.le_one
end MyNat

namespace WorldLeqLevel11
  open MyNat

  theorem le_two : (x : MyNat.Nat) → (hx : Leq x two) → x = zero ∨ x = one ∨ x = two :=
    λ x ↦
      x.casesOn
        (zero := Function.const _ $ Or.inl rfl)
        (succ := λ x' ↦
          x'.casesOn
            (zero := Function.const _ ∘ Or.inr ∘ Or.inl $ rfl)
            (succ := λ n (hsn : Leq (succ (succ n)) two) ↦
              Or.inr ∘ Or.inr
                $ let ⟨z, (hnz : two = add (succ (succ n)) z)⟩ := hsn
                /-
                goal : succ (succ n) = two

                `  add (succ (succ n)) z   =            two  [by Eq.symm hnz]`
                `→ succ (add (succ n)  z)  =            two  [by succ_add]`
                `→ succ (succ (add n   z)) =            two  [by succ_add]`
                `→ succ (succ (add n   z)) = succ (succ two) [by Eq.refl] (normalization)`
                `→             add n   z   =            zero [by succ_inj ∘ succ_inj]`
                `→                 n       =            zero [by add_right_eq_zero]`
                `→      succ (succ n)      =            two  [by congrArg (succ ∘ succ)] (normalization)`
                -/
                let hEq
                  : succ (add (succ n) z) = two
                  := flip Eq.mp (Eq.symm hnz) $ congrArg
                    (flip Eq _)
                    $ succ_add ..

                let hEq
                  : succ (succ (add n z)) = succ (succ zero)
                  := flip Eq.mp hEq $ congrArg
                    (flip Eq _ ∘ succ)
                    $ succ_add ..

                let hEq
                  : add n z = zero
                  := succ_inj ∘ succ_inj $ hEq

                let hEq
                  : n = zero
                  := add_right_eq_zero _ _ hEq

                congrArg
                  (succ ∘ succ)
                  hEq))
end WorldLeqLevel11

namespace MyNat
  def le_two := WorldLeqLevel11.le_two
end MyNat
