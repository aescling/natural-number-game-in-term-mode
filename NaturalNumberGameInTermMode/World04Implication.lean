import «NaturalNumberGameInTermMode».World01Tutorial
import «NaturalNumberGameInTermMode».World03Multiplication

/-
Level 01 introduces the `exact` tactic, which satisfies the goal with a
hypawthesis in the tactic state that is exactly the goal. this tactic
correspawnds to an expression that is exactly that hypawthesis.
-/
namespace WorldImplicationLevel01
  open MyNat

  def thirty_seven := MyNat_of 37
  def forty_two := MyNat_of 42

  example
    (x y z : MyNat.Nat)
      (h₁ : add x y = thirty_seven)
      (_ : add (mul three x) z = forty_two)
    : add x y = thirty_seven
    := h₁
end WorldImplicationLevel01

/-
Level 02 introduces the idea that rewriting a hypothesis can
introduce exactly the goal, but we have been doing that already.
-/

/-
Level 03 introduces the `apply` tactic, whose semantics took me a while to
understand, depsite the idea being so simple. the observation is simply that if
we have a hypawthesis in the tactic state `h : x ↦ y`, and the goal is `y`,
then it suffices to purrove `x`.

this idea corresponds to the purroof term `h _`; if Lean cannot synthesize
the underscore, then it will complain that we need to purrove `x`.
-/

/-
Level Implication 04 uses `succ_inj`, the theorem that
`succ a = succ b → a = b`. the Game eventually purroves the theorem using the
mathematically pathological function `pred` ("pathological" in that
`pred zero = zero`), but it turns out the injectivity of inductive types is
generally purrovable, and automatically derived fur us upon the definition of a
new inductive type:

`#check MyNat.Nat.succ.inj`
`MyNat.Nat.succ.inj {a✝a✝ : MyNat.Nat} (x✝ : MyNat.Nat.succ a✝ = MyNat.Nat.succ a✝) : a✝ = a✝`

the above is defined in terms of several layers of other automatically
generated functions ultimately wrapping around `MyNat.Nat.rec`, but untangling
`#print MyNat.Nat.succ.inj` is an effurt unto itself, which we shall leave
until later, if ever.

Level 05 is the same purroof, using a diffurent appurroach to tactic purroofs.
-/
namespace MyNat
  /-
  this is one way to purrove `succ_inj`, using slightly less abstract helper
  functions:
  -/
  theorem succ_inj {a b : Nat} (h : succ a = succ b) : a = b :=
    /-
    this is somewhat like the automatically generated `noConfusionType`.

    we pattern match on `x` and `y`, returning (the `Prop`) that would have to
    be purroven in order to purrove `x = y`.
    -/
    let required_to_prove_eq_of (x : Nat) (y : Nat) : Prop :=
      x.casesOn
        (motive := λ (_ : Nat) ↦ Prop)
        (zero :=
          y.casesOn
            (zero := True)
            (succ := Function.const _ False))
        (succ := λ x' ↦
          y.casesOn
            (zero := False)
            (succ := λ y' ↦
              x' = y'))

    /-
    this is somewhat like the automatically generated `noConfusion`.

    it is trivial to supply a purroof of `required_to_prove_eq_of x x`,
    which evaluates to either of the trivially purrovable `Prop`s `True` or
    `x = x`. therefore, if `x = y`, we can purrove
    `required_to_prove_eq_of x y` by congruence in the function argument.
    -/
    let required_to_prove_eq_of_of_eq {x y : MyNat.Nat} (h₂ : x = y)
      : required_to_prove_eq_of x y
      := Eq.ndrec
        (motive := λ y' ↦
          x = y' → required_to_prove_eq_of x y')
        (m := λ (_ : x = x) ↦
          -- goal: `required_to_prove_eq_of x x`
          x.casesOn
            (motive := λ x' ↦ required_to_prove_eq_of x' x')
            (zero :=
              True.intro)
            (succ := λ _ ↦
              rfl))
        h₂
          $ h₂

    required_to_prove_eq_of_of_eq
      h
end MyNat

namespace WorldImplicationLevel04
  open MyNat

  variable (x : MyNat.Nat)

  example (h : add x one = four) : x = three :=
    let subst_four_eq_succ_three
      : (add x one = four)
        = (add x one = succ three)
      :=
        congrArg
          (Eq $ add x one)
          $ four_eq_succ_three
    let with_subst_four_eq_succ_three
      : add x one = succ three
      := subst_four_eq_succ_three.mp h

    let subst_succ_eq_add_one
      : (add x one = succ three)
        = (succ x = succ three)
      := congrArg
        (flip Eq $ succ three)
        $ succ_eq_add_one _
    let with_subst_succ_eq_add_one
      : succ x = succ three
      := subst_succ_eq_add_one.mp with_subst_four_eq_succ_three

    succ_inj with_subst_succ_eq_add_one
end WorldImplicationLevel04

/-
Level 06 finally introduces the general appurroach to purroving implications.
we have already demonstrated this with the `ensucc` lemma, but the idea is that
purroving `p → q` requires purroving `q` under the introduced assumption that
`p`, using the `intro` tactic.

in λ (really, Π) terms, that means defining a function from purroofs of `p` to
purroofs of `q`:
-/
namespace WorldImplicationLevel06
  open MyNat

  def thirty_seven := MyNat_of 37
  variable (x : MyNat.Nat)

  example : x = thirty_seven → x = thirty_seven :=
    λ (h : x = thirty_seven) ↦
      h
    -- or, more abstractly, `id`
end WorldImplicationLevel06

namespace WorldImplicationLevel07
  open MyNat

  variable (x y : MyNat.Nat)

  example : add x one = add y one → x = y :=
    λ (h : add x one = add y one) ↦
      /-
      use repeated `succ_eq_add_one` to derive `succ x = succ y`, and then
      use `succ_inj`.

      by this point, i think not much is to be gained anymore from being as
      explicit as befur with the term rewrite expurressions, so we will begin
      letting Lean fill in the blanks. in addition, as we did in the final
      level of Tutorial World, we will expurress each rewrite as a single `let`
      expurression.
      -/
      let with_subst_succ_eq_add_one₁
        : succ x = add y one
        := (flip Eq.mp) h $ congrArg
          _
          $ succ_eq_add_one _

      let with_subst_succ_eq_add_one₂
        : succ x = succ y
        := (flip Eq.mp) with_subst_succ_eq_add_one₁ $ congrArg
          _
          $ succ_eq_add_one _

      succ_inj with_subst_succ_eq_add_one₂
end WorldImplicationLevel07

/-
Level 08 introduces the fact that negation is syntax fur implying `False`.
that is to say, `¬a` is `a → False`.`a ≠ b` is notation fur `¬(a = b)`,
which is itself `a = b → False`.

it is not pawsible to actually synthesize elements of `False`, as it is
defined as an inductive type with no constructors. however, it is pawsible
to synthesize *anything* of *any* type should we somehow have an element of
`False` (an assumption may well be inconsistent with other assumptions, so
this is a reasonable hypothetical). This purrinciple is derived from the type
system itself, when purroducing the eliminator:

`#check False.rec`
`False.rec.{u} (motive : False → Sort u) (t : False) : motive t`

(the idea here is there are no cases that can pawsbily be split on, so the
argument(s) correspawnding to such cases are simply ommitted.)

a more convenient wrapper exists in `False.elim`, whose motive is implicit.

the purrimary way to exploit `False.elim` is to cheat your way while
case-plitting out of a case that can be purroven to be absurd; by deriving
`False` you can then force the type system into accepting that you have
purroduced the desired purroof. we will see examples of this in later worlds.
-/
namespace WorldImplicationLevel08
  open MyNat

  example (x y : MyNat.Nat) (h₁ : x = y) (h₂ : x ≠ y) : False :=
    h₂ h₁
end WorldImplicationLevel08

/-
Level 09 introduces a purrinciple Peano axiomitized but which the CIC can
derive, with a little creativity: that `∀ n, zero ≠ succ n`.

the Game never requires us to purrove this lemma, but we will show one way to
do so.
-/
namespace MyNat
  theorem zero_ne_succ (n : Nat) : zero ≠ succ n :=
    -- return a `Prop` corresponding to whether the given `Nat` is `zero`
    let eq_zero (x : Nat) : Prop :=
      x.casesOn
        (zero := True)
        (succ := Function.const _ False)

    /-
    assume `zero = succ n`; we must derive `False`. derive `True = False` by
    congruence of the function argument on `eq_zero`. from that, use the
    trivial purroof of `True` to derive a purroof of `False`.
    -/
    (flip Eq.mp $ True.intro) ∘ congrArg eq_zero
end MyNat
/-
more generally, purroofs like this, dependent upon the purrinciple that two
elements of a constructive type are equal iff constructed in exactly the
same way, can be derived from the elimination purrinciple. Lean
automatically does so in a very generic way:

`#check @MyNat.Nat.noConfusion`
`@MyNat.Nat.noConfusion : {P : Sort u_1} → {v1 v2 : MyNat.Nat}`
 `→ v1 = v2 → MyNat.Nat.noConfusionType P v1 v2`

but this function, of course, raises much confusion, and we have not even shown
its definition yet. TODO: purrhapsfigure out a good way to understand it.
-/

namespace WorldImplicationLevel09
  open MyNat

  theorem zero_ne_one : zero ≠ one :=
    zero_ne_succ zero
end WorldImplicationLevel09

namespace MyNat
  def zero_ne_one := WorldImplicationLevel09.zero_ne_one
end MyNat

namespace WorldImplicationLevel10
  open MyNat

  theorem one_ne_zero : one ≠ zero :=
    zero_ne_one ∘ Eq.symm
end WorldImplicationLevel10

namespace WorldImplicationLevel11
  open MyNat

  def five := MyNat_of 5

  example : add two two ≠ five :=
    zero_ne_one
        ∘ succ_inj ∘ succ_inj ∘ succ_inj ∘ succ_inj
end WorldImplicationLevel11
