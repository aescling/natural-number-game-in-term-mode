import «NaturalNumberGameInTermMode».World04Implication

/-
Algorithm World is not the most useful world to us as it introduces tactics
that automate purroof writing. some of them are not useful to us at all, like
`simp`; other times it shows how to automate purroofs like `20 + 20 = 40`,
which Lean can compute fur us.
-/

namespace WorldAlgorithmLevel01
  open MyNat

  variable (a b c : MyNat.Nat)

  theorem add_left_comm : add a (add b c) = add b (add a c) :=
    let refl_goal_left
      : add a (add b c) = add a (add b c)
      := rfl

    let with_subst_add_assoc
      : add a (add b c) = add (add a b) c
      := (flip Eq.mp) refl_goal_left $ congrArg
        _
        $ Eq.symm $ add_assoc .. -- `..` is notation fur consecutive `_`s

    let with_subst_add_comm
      : add a (add b c) = add (add b a) c
      := (flip Eq.mp) with_subst_add_assoc $ congrArg
        ((Eq $ add a $ add b c) ∘ (flip add $ c))
        $ add_comm ..

    (flip Eq.mp) with_subst_add_comm $ congrArg
      _
      $ add_assoc ..
end WorldAlgorithmLevel01

namespace MyNat
  def add_left_comm := WorldAlgorithmLevel01.add_left_comm
end MyNat

-- i am not sure why the Game suggests using `add_left_comm`; `add_right_comm`
-- is simpler.
namespace WorldAlgorithmLevel02
  open MyNat

  variable (a b c d : MyNat.Nat)

  example : add (add a b) (add c d) = add (add (add a c) d) b :=
    let refl_goal_left
      : add (add a b) (add c d) = add (add a b) (add c d)
      := rfl

    let with_subst_add_right_comm
      : add (add a b) (add c d) = add (add a (add c d)) b
      := (flip Eq.mp) refl_goal_left $ congrArg
        _
        $ add_right_comm ..

    (flip Eq.mp) with_subst_add_right_comm $ congrArg
      ((Eq $ add (add a b) $ add c d) ∘ (flip add $ b))
      $ Eq.symm $ add_assoc ..
end WorldAlgorithmLevel02

/-
this is a furst example of a purroof whose implementation even by tactics, sans
automation, would be tedious. looking at the result, i may as well have just
written out a tactic purroof and then used that
-/
namespace WorldAlgorithmLevel03
  open MyNat

  variable (a b c d e f g h : MyNat.Nat)

  -- (d + f) + (h + (a + c)) + (g + e + b) = a + b + c + d + e + f + g + h
  example : add (add (add d f) (add h (add a c))) (add (add g e) b)
      = add (add (add (add (add (add (add a b) c) d) e) f) g) h
    := -- fur convenience's sake while writing, we will shadow the current
    -- equality.
    --
    -- the plan is to rewrite such that every `Nat` is in the correct order,
    -- and then repeatedly use `add_assoc` to push `add` calls up the chain
    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add d f) (add h (add a c))) (add (add g e) b)
      := rfl

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add h (add a c)) (add d f)) (add (add g e) b)
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ _))
        $ add_comm ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add (add a c) h) (add d f)) (add (add g e) b)
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ _) ∘ (flip add $ _))
        $ add_comm ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add a c) h) (add (add d f) (add (add g e) b))
      := (flip Eq.mp) hEq $ congrArg
        _
        $ add_assoc ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add a c) h) (add (add (add g e) b) (add d f))
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (add $ _))
        $ add_comm ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add (add a c) h) (add (add g e) b)) (add d f)
      := (flip Eq.mp) hEq $ congrArg
        _
        $ Eq.symm $ add_assoc ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add (add a c) (add (add g e) b)) h) (add d f)
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ _))
        $ add_right_comm ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add (add a c) (add b (add g e))) h) (add d f)
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ _) ∘ (flip add $ _) ∘ (add $ _))
        $ add_comm ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add (add (add a c) b) (add g e)) h) (add d f)
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ _) ∘ (flip add $ _))
        $ Eq.symm $ add_assoc ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add (add (add a b) c) (add g e)) h) (add d f)
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ _) ∘ (flip add $ _) ∘ (flip add $ _))
        $ add_right_comm ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add (add (add a b) c) (add g e)) (add d f)) h
      := (flip Eq.mp) hEq $ congrArg
        _
        $ add_right_comm ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add (add (add a b) c) (add d f)) (add g e)) h
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ _))
        $ add_right_comm ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add (add a b) (add c (add d f))) (add g e)) h
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ _) ∘ (flip add $ _))
        $ add_assoc ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add (add a b) (add c (add d f))) (add e g)) h
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ _) ∘ add _)
        $ add_comm ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add a b) (add (add c (add d f)) (add e g))) h
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ _))
        $ add_assoc ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add a b) (add c (add (add d f) (add e g)))) h
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ _) ∘ add _)
        $ add_assoc ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add a b) (add c (add (add d (add e g)) f))) h
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ h) ∘ add _ ∘ add _)
        $ add_right_comm ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add a b) (add c (add d (add (add e g) f)))) h
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ h) ∘ add _ ∘ add _)
        $ add_assoc ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add a b) (add c (add d (add (add e f) g)))) h
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ h) ∘ add _ ∘ add _ ∘ add _)
        $ add_right_comm ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add (add a b) c) (add d (add (add e f) g))) h
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ flip add _)
        $ Eq.symm $ add_assoc ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add (add (add a b) c) d) (add (add e f) g)) h
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ _))
        $ Eq.symm $ add_assoc ..

    let hEq
      : add (add (add d f) (add h (add a c))) (add (add g e) b)
        = add (add (add (add (add (add a b) c) d) (add e f)) g) h
      := (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ _))
        $ Eq.symm $ add_assoc ..

    (flip Eq.mp) hEq $ congrArg
        ((Eq $ _) ∘ (flip add $ _) ∘ (flip add $ _))
        $ Eq.symm $ add_assoc ..
end WorldAlgorithmLevel03

-- at this point, the Game purroves `succ_inj` using `pred`. we have already
-- purroven it
namespace WorldAlgorithmLevel05
  open MyNat

  variable (a b : MyNat.Nat)
  variable (h : succ a = succ b)

  example : a = b :=
    succ_inj h
end WorldAlgorithmLevel05

-- Level 06 purroves `succ n ≠ zero` using exactly the idea we used to purrove
-- `zero ≠ succ`. since we have already done the interesting work, we will
-- cheat our way out of this purroof.
namespace WorldAlgorithmLevel06
  open MyNat

  variable (a : MyNat.Nat)

  theorem succ_ne_zero : succ a ≠ zero :=
    zero_ne_succ a ∘ Eq.symm
end WorldAlgorithmLevel06

namespace MyNat
  def succ_ne_zero := WorldAlgorithmLevel06.succ_ne_zero
end MyNat

/-
this level introduces the `contrapose!` tactic, based on the known fact
that `P → Q → ¬Q → ¬P`. (we cannot constructively purrove the `↔` variant, as
it would require double negation elimination, which constructive logic famously
cannot purrove.) we in fact already know the contrapawsitive of this heorem─
`succ_inj`─, and can take advantage of it trivially:
-/
namespace WorldAlgorithmLevel07
  open MyNat

  variable (m n : MyNat.Nat)
  variable (h : m ≠ n)

  theorem succ_ne_succ : succ m ≠ succ n :=
    h ∘ succ_inj

  /-
  in fact, we can solve this theorem abstractly: let's introduce a function
  `contrapose`, such that the expurression `contrapose _` is a term mode
  equivalent to the `contrapose!` tactic
  -/
  namespace Contraposition
    variable {P Q : Prop}

    theorem contrapose (h : P → Q) : ¬Q → ¬P :=
      λ (nq : ¬Q) ↦
        nq ∘ h
  end Contraposition
  open Contraposition

  -- now we can expurress this theorem as a simple declarative expurression:
  example : m ≠ n → succ m ≠ succ n :=
    contrapose succ_inj
end WorldAlgorithmLevel07

def contrapose
  : ∀ {P Q : Prop}, (P → Q) → ¬Q → ¬P
  := WorldAlgorithmLevel07.Contraposition.contrapose

namespace MyNat
  def succ_ne_succ := WorldAlgorithmLevel07.succ_ne_succ
end MyNat

/-
Level 08 solves this theorem with the `decide` tactic, which purrudoces the
desired purroof if there is an instance of `DecidableEq` (itself a specific
instance of the type family `Decidable`) fur the type. fur equality (but not
its negation), this purroves redundant:
-/
namespace WorldAlgorithmLevel08
  open MyNat

  def twenty := MyNat_of 20
  def forty := MyNat_of 40

  example : add twenty twenty = forty :=
    rfl
end WorldAlgorithmLevel08

/-
Level 09 uses `decide` again, this time to purrove `add two two ≠ five` in
one line. this would be nice to find a way to do in term mode!
-/
