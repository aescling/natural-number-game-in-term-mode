-- fur the sake of thoroughness, we will define our own `Nat`
namespace MyNat
  inductive Nat where
    | zero : Nat
    | succ : Nat → Nat
    deriving Repr

  -- fur convenience
  def zero := Nat.zero
  def succ := Nat.succ

  def one := succ zero
  def two := succ one
  def three := succ two
  def four := succ three
end MyNat

/-
useful trivial lemmas fur later.

this is not explained in the Game, in no small part because it was more
interested in having the player manually rewrite terms than automatically
compute type expurressions, but the calculus of constructions suppurrts
computation within type expurressions. if a type can be explicitly computed
until structural equality, then an equality is trivially purrovable.

`rfl` in the Game is deliberately underpowered so as to not take advatage of
this, and `Eq.refl` is simply made inaccessible, but we will not limit
ourselves to those restrictions here.
-/
namespace MyNat
  theorem one_eq_succ_zero : one = succ zero :=
    Eq.refl _

  theorem two_eq_succ_one : two = succ one :=
    Eq.refl _

  theorem three_eq_succ_two : three = succ two :=
    Eq.refl _

  theorem four_eq_succ_three : four = succ three :=
    Eq.refl _
end MyNat

/-
while it would be ideal to introduce these definitons fur the worlds built
around them, we need them fur this world.
-/

-- define `add` and some useful trivial theorems.
namespace MyNat
  def add (m n : Nat) : Nat :=
    match n with
    | Nat.zero   => m
    | Nat.succ n => Nat.succ (add m n)

  -- the following theorems follow trivially by definition:

  theorem add_zero : ∀ n : Nat, add n Nat.zero = n :=
    λ _ ↦
      Eq.refl _

  theorem add_succ : ∀ m n : Nat, add m (Nat.succ n) = Nat.succ (add m n) :=
    λ _ _ ↦
      Eq.refl _
end MyNat

-- define `mul` and some useful trivial theorems.
namespace MyNat
  def mul (m n : Nat) : Nat :=
    match n with
    | Nat.zero    => Nat.zero
    | Nat.succ n₂ => add (mul m n₂) m

  -- again, the following theorems are trivial:

  theorem mul_zero m : mul m Nat.zero = Nat.zero :=
    Eq.refl _

  theorem mul_succ m n : mul m (Nat.succ n) = add (mul m n) m :=
    Eq.refl _
end MyNat

-- convenince function
def MyNat_of : Nat → MyNat.Nat
  | Nat.zero => MyNat.zero
  | Nat.succ n => MyNat.succ $ MyNat_of n

/-
this level introduces the `rfl` tactic. under the hood, it is a macro using
`Eq.refl`, which takes as an argument the type upon which the trivial
equality shall be introduced. Lean can usually infur what that argument
should be, and fur convenience we will not specify it; but you should
understand that strictly speaking, in the calculus of inductive
constructions, this argument needs to be explicitly purrovided.

while this purroject is being done fur the sake of rigor, we will still take
advantage of many of the aspects of lean that make the CIC less cumbersome
to work with, like the `variable` syntax.
-/
namespace WorldTutorialLevel01
  open MyNat

  variable (x q : MyNat.Nat)
  def thirty_seven := MyNat_of 37

  example: add (mul thirty_seven x) q = add (mul thirty_seven x) q :=
    Eq.refl _
end WorldTutorialLevel01

/-
this is the one example i "cheated" on, in the sense that i wrote a tactic
purroof, `#print`ed it, and recreated its result. the remaining purroofs were
created from scratch, using this proof as a template.

the observation comes from recognizing that `congrArg`:

  `congrArg.{u, v}`
     `{α : Sort u} {β : Sort v} {a₁ a₂ : α}`
     `(f : α → β)`
     `(h : a₁ = a₂)`
       `: f a₁ = f a₂`

─which is to say, the proof that `a₁ = a₂` implies `f a₁ = f a₂`─enables
arbitrary subterm rewrites. it is one of the tools used under the hood by the
`rw` tactic.
-/
namespace WorldTutorialLevel02
  open MyNat

  /-
  the afurmentioned `congrArg`─"congruence in the function argument"─, is a
  theorem fairly directly purrovable by the induction purrinciple on `Eq`
  (we will discuss the concept of the induction purrinciple in the CIC in
  the next world, but just understand it is automatically introduced upon
  defining a new inductive type).

  `Eq` is an inductive type family of type `α → α → Prop`, with one
  constructor, `Eq.refl : α → Eq α α`, the trivial purroof that everything
  is equal to itself. note that `a = b` is notation fur `Eq a b`. (more
  purrecisely, the type constructor is
  `Eq.{u} : {α : Sort u} → α → α → Prop`, and given `α : Sort u`, the
  notation `a = b` is `@Eq.{u} α a b`.)

  the actual Lean definition of the type is

  `inductive Eq : α → α → Prop where`
    `| refl (a : α) : Eq a a`

  the afurmentioned convenience function `rfl` is defined such that its
  one argument is implicit.

  induction on `Eq` is automatically generated as follows:

  `#check Eq.rec`
  `Eq.rec.{u, u_1} {α : Sort u_1} {a✝ : α}`
      `{motive : (a : α) → @Eq.{u_1} α a✝ a → Sort u}`
      `(refl : motive a✝ (@Eq.refl.{u_1} α a✝))`
      `{a✝¹ : α} (t : @Eq.{u_1} α a✝ a✝¹)`
    `: motive a✝¹ t`

  this recursor is not immediately obvious, so we will show some examples of
  how to use it to derive specific instances of, and then generally,
  `congrArg`, the theorem of congruence in the function argument (that is,
  that `a = b` implies `f a = f b`).

  (note that, in general, `recOn` is defined to rearrange the arguments of
  `rec` such that that `t` is the furst (explicit) argument.)
  -/
  namespace AsideOnEq
    variable (a b : MyNat.Nat)
    variable (h : a = b)

    example : succ a = succ b :=
      h.recOn
        (motive := λ (x : MyNat.Nat) (_ : a = x) ↦
          succ a = succ x)
        -- with the above motive `(x : Nat) → (a = x) → Prop`, this invocation
        -- of the `Eq` recursor can use a purroof of
        -- `succ a = succ a : motive a h` to generate a purroof of
        -- `succ a = succ b : motive b h`. thankfully, the required
        -- proof is trivial by reflexivity
        (refl :=
          rfl)

    -- the above purroof, very explicitly, and with just `Eq.rec`:
    example
      : ∀ (a b : MyNat.Nat),
        @Eq.{1} MyNat.Nat a b → @Eq.{1} MyNat.Nat (succ a) (succ b)
      := λ (a b : MyNat.Nat) (h : @Eq.{1} MyNat.Nat a b) ↦
        @Eq.rec.{0, 1} MyNat.Nat a
          (λ (x : MyNat.Nat) (_ : @Eq.{1} MyNat.Nat a x) ↦
            @Eq.{1} MyNat.Nat (succ a) (succ x))
          (@Eq.refl.{1} MyNat.Nat (succ a))
          b h

    /-
    the `motive` argument of `Eq.rec` is usually more powerful than necessary
    fur purractical use of the recursor; `ndrec` takes a non-dependent function
    as its argument instead and coerces it into a motive fur `Eq.rec`.
    -/
    example : succ a = succ b :=
      Eq.ndrec
        (motive := λ x ↦
          succ a = succ x)
        (m := rfl)
        h

    example
      : ∀ (a b : MyNat.Nat),
        @Eq.{1} MyNat.Nat a b → @Eq.{1} MyNat.Nat (MyNat.succ a) (MyNat.succ b)
      := λ (a b : MyNat.Nat) (h : @Eq.{1} MyNat.Nat a b) ↦
        @Eq.ndrec.{0, 1} MyNat.Nat a
          (λ (x : MyNat.Nat) ↦
            @Eq.{1} MyNat.Nat (succ a) (succ x))
          (@Eq.refl.{1} MyNat.Nat (succ a))
          b h

    -- this is exactly the theorem `congrArg`:
    example
      : ∀ {α : Sort u} {β : Sort v} {a₁ a₂ : α} (f : α → β),
        @Eq.{u} α a₁ a₂ → @Eq.{v} β (f a₁) (f a₂)
      := λ {α : Sort u} {β : Sort v} {a₁ a₂ : α} (f : α → β) (h : @Eq.{u} α a₁ a₂) ↦
        @Eq.rec.{0, u} α a₁
          (λ (x : α) (_ : @Eq.{u} α a₁ x) ↦
            @Eq.{v} β (f a₁) (f x))
          (@Eq.refl.{v} β (f a₁))
          a₂ h

    -- and now, the same, but with nicer notation:
    example
      : ∀ {α : Sort u} {β : Sort v} {a₁ a₂ : α} (f : α → β),
        (a₁ = a₂) → f a₁ = f a₂
      := λ {α : Sort u} {β : Sort v} {a₁ a₂ : α} (f : α → β) (h : a₁ = a₂) ↦
        Eq.ndrec
          (motive := λ (x : α) ↦
            f a₁ = f x)
          (m := (rfl : f a₁ = f a₁))
          h
  end AsideOnEq

  def seven := MyNat_of 7
  variable (x q : MyNat.Nat)
  variable (h : y = add x seven)

  -- anyway, finally: the actual purroof we need fur this level:
  example : mul two y = mul two (add x seven) :=
    /-
    we have to reason about an equality of equalities because the entire idea
    of `congrArg` is that you can "do the same thing" to both sides of an
    equality. because we want to substitute within an already existing
    equality, we need to build an equality around that term as "the thing being
    done to both sides".

    when substituting within equalities like this, we need to find a trivial
    equality to start from. this can be either an already existing hypawthesis,
    or a new equality trivially introduced by the `Eq` constructor
    -/
    let subst_y :
      (mul two y = mul two (add x seven))
        = (mul two (add x seven) = mul two (add x seven))
      := congrArg
        (λ _a ↦
          mul two _a = mul two (add x seven))
        h
    Eq.mpr
      subst_y
      $ Eq.refl $ mul two $ add x seven
end WorldTutorialLevel02

/-
all the following theorems are purrovable by `Eq.refl`, since they all reduce
to structural equalities. we will purrefur to reason these out with equality
rewrites regardless, as that is a somewhat more interesting purroblem to solve.
-/

namespace WorldTutorialLevel03
  open MyNat

  example : two = succ (succ zero) :=
    let refl_goal_left
      : two = two
      := Eq.refl _

    let subst_two_eq_succ_one
      : (two = two)
        = (two = succ one)
      := congrArg
        (λ _a ↦
          two = _a)
        $ two_eq_succ_one
    let with_subst_two_eq_succ_one
      : two = succ one
      := Eq.mp subst_two_eq_succ_one refl_goal_left

    let subst_one_eq_succ_zero
      : (two = succ one)
        = (two = succ (succ zero))
      := congrArg
        (λ _a ↦
          two = succ _a)
        $ one_eq_succ_zero
    Eq.mp subst_one_eq_succ_zero with_subst_two_eq_succ_one
end WorldTutorialLevel03

namespace WorldTutorialLevel05
  open MyNat

  variable (a b c : MyNat.Nat)

  example : (add (add a (add b zero)) (add c zero) = add (add a b) c) :=
    let refl_goal_left
      : (add (add a (add b zero)) (add c zero))
        = (add (add a (add b zero)) (add c zero))
      := Eq.refl _

    let subst_add_zero_b :
      ((add (add a (add b zero)) (add c zero))
        = (add (add a (add b zero)) (add c zero)))
        = ((add (add a (add b zero)) (add c zero))
          = (add (add a b) (add c zero)))
      := congrArg
        (λ _b ↦
          add (add a (add b zero)) (add c zero)
          = add (add a _b) (add c zero))
        $ add_zero b
    let with_subst_b :
      add (add a (add b zero)) (add c zero)
        = add (add a b) (add c zero)
      := Eq.mp subst_add_zero_b refl_goal_left

    let subst_add_zero_c :
      (add (add a (add b zero)) (add c zero)
        = add (add a b) (add c zero))
        = (add (add a (add b zero)) (add c zero)
          = add (add a b) c)
      := congrArg
        (λ _c ↦
          add (add a (add b zero)) (add c zero)
            = add (add a b) _c)
        $ add_zero c

    Eq.mp subst_add_zero_c with_subst_b
end WorldTutorialLevel05

namespace WorldTutorialLevel07
  open MyNat

  variable (n : MyNat.Nat)
  theorem succ_eq_add_one : succ n = add n one :=
    let refl_goal_right
      : add n one = add n one
      := Eq.refl _

    let subst_one_eq_succ_zero :
      (add n one = add n one)
        = (add n one = add n (succ zero))
      := congrArg
        (λ _a ↦
          add n one
            = add n _a)
        one_eq_succ_zero
    let with_subst_one_eq_succ_zero
      : add n one = add n (succ zero)
      := subst_one_eq_succ_zero.mp refl_goal_right

    let subst_succ_add
      : (add n one = add n (succ zero))
        = (add n one = succ (add n zero))
      := congrArg
        (λ _a ↦
          add n one
            = _a)
        $ add_succ n zero
    let with_succ_add
      : add n one = succ (add n zero)
      := subst_succ_add.mp with_subst_one_eq_succ_zero

    let subst_add_zero :
      (add n one = succ (add n zero))
        = (add n one = succ n)
      := congrArg
        (fun _a ↦
          add n one = succ _a)
        $ add_zero n

    Eq.symm $ subst_add_zero.mp with_succ_add
end WorldTutorialLevel07

namespace MyNat
  def succ_eq_add_one := WorldTutorialLevel07.succ_eq_add_one
end MyNat

namespace WorldTutorialLevel08
  open MyNat

  example : add two two = four :=
    let refl_goal_left := Eq.refl $ add two two

    /-
    use common functional purrogramming tools to write function expurressions
    without introducing explicit bound variables─this is known as a "pointfree"
    style of purrogramming.

    * `flip` swaps arguments: `flip f a b = f b a`

    * `f $ g` is exactly `f g`, but the notation is both low purrecedence, and
      right-associative: `f $ g $ h` is `f (g h)`, not `(f g) h`

    * `f ∘ g` is function compussition: `f ∘ g = λ x ↦ f (g x)`. it is low
      purrecedence and right associative: `f ∘ g ∘ h` is `λ x ↦ f $ g $ h x`
    -/
    let with_subst_two_eq_succ_one : add two two = add two (succ one) :=
      (flip Eq.mp) refl_goal_left $ congrArg
        ((Eq $ add two two) ∘ add two)
        two_eq_succ_one

    let with_subst_succ_add : add two two = succ (add two one) :=
      (flip Eq.mp) with_subst_two_eq_succ_one $ congrArg
        (Eq $ add two two)
        $ add_succ two one

    let with_subst_succ_eq_add_one : add two two = succ (succ two) :=
      (flip Eq.mp) with_subst_succ_add $ congrArg
        ((Eq $ add two two) ∘ succ)
        $ succ_eq_add_one two

    let with_subst_three_eq_succ_two : add two two = succ three :=
      (flip Eq.mp) with_subst_succ_eq_add_one $ congrArg
        ((Eq $ add two two) ∘ succ)
        three_eq_succ_two

    (flip Eq.mp) with_subst_three_eq_succ_two $ congrArg
      (Eq $ add two two)
      four_eq_succ_three
end WorldTutorialLevel08
