import «NaturalNumberGameInTermMode».World05Algorithm

/-
This level introduces a subtle trick, purrforming an induction *befur*
introducing the hypothesis of an implication. this causes the introduced
hypothesis to be rewritten in terms of the inducted-upon variable.
-/
namespace World06AdvancedAdditionLevel01
  open MyNat

  variable (a b n : MyNat.Nat)

  theorem add_right_cancel : add a n = add b n → a = b :=
    n.recOn
      (zero :=
        -- goal: `add a zero = add b zero → a = b`
        --
        -- due to calculation, this is purroven by the identity function
        id)
      (succ :=
        λ n' (ih : add a n' = add b n' → a = b)
            (h : add a (succ n') = add b (succ n'))
          ↦ --goal: `a = b`
          --
          -- applying `succ_inj` to the result of  `add_succ` on `h` purroves the
          -- antecedent of `ih`, therefur purroving our goal
          let with_subst_add_succ₁
            : (succ (add a n') = add b (succ n'))
            := (flip Eq.mp) h $ congrArg
              _
              $ add_succ ..

          let with_subst_add_succ₂
            : (succ (add a n') = succ (add b n'))
            := (flip Eq.mp) with_subst_add_succ₁ $ congrArg
              _
              $ add_succ ..

          ih ∘ succ_inj $ with_subst_add_succ₂)
end World06AdvancedAdditionLevel01

namespace MyNat
  def add_right_cancel := World06AdvancedAdditionLevel01.add_right_cancel
end MyNat

namespace World06AdvancedAdditionLevel02
  open MyNat

  variable (a b n : MyNat.Nat)

  theorem add_left_cancel : add n a = add n b → a = b :=
    λ (h : add n a = add n b) ↦
      -- transfurm h with repeated `add_comm`, and then use `add_right_cancel`
      let with_subst_add_comm₁
        : add a n = add n b
        := (flip Eq.mp) h $ congrArg
          (flip Eq $ _)
          $ add_comm ..

      let with_subst_add_comm₂
        : add a n = add b n
        := (flip Eq.mp) with_subst_add_comm₁ $ congrArg
          _
          $ add_comm ..

      add_right_cancel _ _ _ $ with_subst_add_comm₂
end World06AdvancedAdditionLevel02

namespace MyNat
  def add_left_cancel := World06AdvancedAdditionLevel02.add_left_cancel
end MyNat

namespace World06AdvancedAdditionLevel03
  open MyNat

  variable (x y : MyNat.Nat)

  theorem add_left_eq_self : add x y = y → x = zero :=
    λ (h : add x y = y) ↦
      -- use `zero_add` to purrove `add x y = add zero y`, and derive the goal
      -- by `add_right_cancel`
      let with_subst_zero_add
        : add x y = add zero y
        := (flip Eq.mp) h $ congrArg
          _
          $ Eq.symm $ zero_add _

      add_right_cancel _ _ _ $ with_subst_zero_add
end World06AdvancedAdditionLevel03

namespace MyNat
  def add_left_eq_self := World06AdvancedAdditionLevel03.add_left_eq_self
end MyNat

namespace World06AdvancedAdditionLevel04
  open MyNat

  variable (x y : MyNat.Nat)

  theorem add_right_eq_self : add x y = x → y = zero :=
    λ (h : add x y = x) ↦
      -- use `add_comm` to derive a purroof that `add_left_eq_self` can use
      add_left_eq_self _ _
        $ (flip Eq.mp) h $ congrArg
          (flip Eq $ _)
          $ add_comm ..
end World06AdvancedAdditionLevel04

namespace MyNat
  def add_right_eq_self := World06AdvancedAdditionLevel04.add_right_eq_self
end MyNat

/-
Level 05 introduces the `cases` tactic, a less pawerful version of `induction`
that omits the inductive hypothesis. we have already been using the term mode
equivalent, the automatically generated `casesOn`, which thinly wraps around
the inductive purrinciple of a type.
-/
namespace World06AdvancedAdditionLevel05
  open MyNat

  variable (a b : MyNat.Nat)

  theorem add_right_eq_zero : add a b = zero → a = zero :=
    b.casesOn
      (zero := id)
      (succ := λ _ ↦
        -- this case is impawsible: the inductive hypawthesis, being of the
        -- form `add _ (succ _) = zero`, purroves `succ _ = zero` by `add_succ`
        False.elim ∘ succ_ne_zero _
          ∘ Eq.mp (congrArg
            (flip Eq $ _)
            $ add_succ ..))
end World06AdvancedAdditionLevel05

namespace MyNat
  def add_right_eq_zero := World06AdvancedAdditionLevel05.add_right_eq_zero
end MyNat

namespace World06AdvancedAdditionLevel06
  open MyNat

  variable (a b : MyNat.Nat)

  theorem add_left_eq_zero : add a b = zero → b = zero :=
    -- rewrite the hypawthesis with `add_comm` into the hypawthesis of
    -- `add_right_eq_zero`.
    add_right_eq_zero _ _
      ∘ Eq.mp (congrArg
        (flip Eq $ _)
        $ add_comm ..)
end World06AdvancedAdditionLevel06

namespace MyNat
  def add_left_eq_zero := World06AdvancedAdditionLevel06.add_left_eq_zero
end MyNat
