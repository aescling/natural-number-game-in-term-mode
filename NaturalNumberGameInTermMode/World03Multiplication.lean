import NaturalNumberGameInTermMode.World02Addition

namespace WorldMultiplicationLevel01
  open MyNat

  variable (m : MyNat.Nat)

  theorem mul_one : mul m one = m :=
    /-
    `  mul m one = mul m one          [by Eq.refl]`
    `→ mul m one = mul m (succ zero)  [by one_eq_succ_zero]`
    `→ mul m one = add (mul m zero) m [by mul_succ]`
    `→ mul m one = add zero m         [by mul_zero]`
    `→ mul m one = m                  [by zero_add]`
    -/

    let refl_goal_left
      : mul m one = mul m one
      := rfl

    let subst_one_eq_succ_zero
      : (mul m one = mul m one)
        = (mul m one = mul m (succ zero))
      := congrArg
        (λ _a ↦
          mul m one = mul m _a)
        $ Eq.symm one_eq_succ_zero
    let with_subst_one_eq_succ_zero
      : mul m one = mul m (succ zero)
      := Eq.mp subst_one_eq_succ_zero refl_goal_left

    let subst_mul_succ
      : (mul m one = mul m (succ zero))
        = (mul m one = add (mul m zero) m)
      := congrArg
        (Eq $ mul m one)
        $ mul_succ m zero
    let with_subst_mul_succ
      : mul m one = add (mul m zero) m
      := Eq.mp subst_mul_succ with_subst_one_eq_succ_zero

    let subst_mul_zero
      : (mul m one = add (mul m zero) m)
        = (mul m one = add zero m)
      := congrArg
        (λ _a ↦
          mul m one = add _a m)
        $ mul_zero m
    let with_subst_mul_zero
      : mul m one = add zero m
      := Eq.mp subst_mul_zero with_subst_mul_succ

    let subst_zero_add
      : (mul m one = add zero m)
        = (mul m one = m)
      := congrArg
        (Eq $ mul m one)
        $ zero_add m
    Eq.mp subst_zero_add with_subst_mul_zero
end WorldMultiplicationLevel01

namespace MyNat
  def mul_one := WorldMultiplicationLevel01.mul_one
end MyNat

namespace WorldMultiplicationLevel02
  open MyNat

  variable (m : MyNat.Nat)

  theorem zero_mul : mul zero m = zero :=
    m.recOn
      (zero :=
        -- goal: `mul zero zero = zero`; trivial
        rfl
      )
      (succ := λ n (ih : mul zero n = zero) ↦
        /-
        goal: `mul zero (succ n) = zero`

        `  mul zero (succ n) = mul zero (succ n)     [by Eq.refl]`
        `→ mul zero (succ n) = add (mul zero n) zero [by mul_succ]`
        `→ mul zero (succ n) = mul zero n            [by add_zero]`
        `→ mul zero (succ n) = zero                  [by ih]`
        -/

        let refl_goal_left
          : mul zero (succ n) = mul zero (succ n)
          := rfl

        let subst_mul_succ
          : (mul zero (succ n) = mul zero (succ n))
            = (mul zero (succ n) = add (mul zero n) zero)
          := congrArg
            (Eq $ mul zero $ succ n)
            $ mul_succ zero n
        let with_subst_mul_succ
          : mul zero (succ n) = add (mul zero n) zero
          := Eq.mp subst_mul_succ refl_goal_left

        let subst_add_zero
          : (mul zero (succ n) = add (mul zero n) zero)
            = (mul zero (succ n) = mul zero n)
          := congrArg
            (Eq $ mul zero $ succ n)
            $ add_zero $ mul zero n
        let with_subst_add_zero
          : mul zero (succ n) = mul zero n
          := Eq.mp subst_add_zero with_subst_mul_succ

        let subst_ih
          : (mul zero (succ n) = mul zero n)
            = (mul zero n = zero)
          := congrArg
            (Eq $ mul zero $ succ n)
            ih
        Eq.mp subst_ih with_subst_add_zero)
end WorldMultiplicationLevel02

namespace MyNat
  def zero_mul := WorldMultiplicationLevel02.zero_mul
end MyNat

namespace WorldMultiplicationLevel03
  open MyNat

  variable (a b : MyNat.Nat)

  theorem succ_mul : mul (succ a) b = add (mul a b) b :=
    b.recOn
      (
        -- goal: `mul (succ a) zero = add (mul a zero) zero`; trivial
        rfl
      )
      (λ n (ih : mul (succ a) n = add (mul a n) n) ↦
        /-
        goal: `mul (succ a) (succ n) = add (mul a (succ n)) (succ n)`

        `  mul (succ a) (succ n) = mul (succ a) (succ n)              [by Eq.refl]`
        `→ mul (succ a) (succ n) = add (mul (succ a) n) (succ a)      [by mul_succ]`
        `→ mul (succ a) (succ n) = add (add (mul a n) n) (succ a)     [by ih]`
        `→ mul (succ a) (succ n) = add (add (mul a n) (succ a)) n     [by add_right_comm]`
        `→ mul (succ a) (succ n) = add (succ (add (mul a n) a)) n     [by add_succ]`
        `→ mul (succ a) (succ n) = succ (add (add (add mul a n) a) n) [by succ_add]`
        `→ mul (succ a) (succ n) = succ (add (mul a (succ n)) n)      [by mul_succ]`
        `→ mul (succ a) (succ n) = add (mul a (succ n)) (succ n)      [by add_succ]`
        -/

        let refl_goal_left
          : mul (succ a) (succ n) = mul (succ a) (succ n)
          := rfl

        let subst_mul_succ₁
          : (mul (succ a) (succ n) = mul (succ a) (succ n))
            = (mul (succ a) (succ n) = add (mul (succ a) n) (succ a))
          := congrArg
            (Eq $ mul (succ a) $ succ n)
            $ mul_succ (succ a) n
        let with_subst_mul_succ
          : mul (succ a) (succ n) = add (mul (succ a) n) (succ a)
          := Eq.mp subst_mul_succ₁ refl_goal_left

        let subst_ih
          : (mul (succ a) (succ n) = add (mul (succ a) n) (succ a))
            = (mul (succ a) (succ n) = add (add (mul a n) n) (succ a))
          := congrArg
            (λ _a ↦
              mul (succ a) (succ n) = add _a (succ a))
            $ ih
        let with_subst_ih
          : mul (succ a) (succ n) = add (add (mul a n) n) (succ a)
          := Eq.mp subst_ih with_subst_mul_succ

        let subst_add_right_comm
          : (mul (succ a) (succ n) = add (add (mul a n) n) (succ a))
            = (mul (succ a) (succ n) = add (add (mul a n) (succ a)) n)
          := congrArg
            (Eq $ mul (succ a) $ succ n)
            $ add_right_comm (mul a n) n (succ a)
        let with_subst_add_right_comm
          : mul (succ a) (succ n) = add (add (mul a n) (succ a)) n
          := Eq.mp subst_add_right_comm with_subst_ih

        let subst_add_succ
          : (mul (succ a) (succ n) = add (add (mul a n) (succ a)) n)
            = (mul (succ a) (succ n) = add (succ (add (mul a n) a)) n)
          := congrArg
            (λ _a ↦
              mul (succ a) (succ n) = add _a n)
            $ add_succ (mul a n) a
        let with_subst_add_succ
          : mul (succ a) (succ n) = add (succ (add (mul a n) a)) n
          := Eq.mp subst_add_succ with_subst_add_right_comm

        let subst_succ_add
          : (mul (succ a) (succ n) = add (succ (add (mul a n) a)) n)
            = (mul (succ a) (succ n) = succ (add (add (mul a n) a) n))
          := congrArg
            (Eq $ mul (succ a) $ succ n)
            $ succ_add (add (mul a n) a) n
        let with_subst_succ_add
           : mul (succ a) (succ n) = succ (add (add (mul a n) a) n)
           := Eq.mp subst_succ_add with_subst_add_succ

        let subst_mul_succ₂
          : (mul (succ a) (succ n) = succ (add (add (mul a n) a) n))
            = (mul (succ a) (succ n) = succ (add (mul a (succ n)) n))
          := congrArg
            (λ _a ↦
              mul (succ a) (succ n) = succ (add _a n))
            $ mul_succ a n
        let with_subst_mul_succ₂
          : mul (succ a) (succ n) = succ (add (mul a (succ n)) n)
          := Eq.mp subst_mul_succ₂ with_subst_succ_add

        let with_subst_add_succ₂
          : (mul (succ a) (succ n) = succ (add (mul a (succ n)) n))
            = (mul (succ a) (succ n) = add (mul a (succ n)) (succ n))
          := congrArg
            (Eq $ mul (succ a) $ succ n)
            $ add_succ (mul a (succ n)) n
        Eq.mp with_subst_add_succ₂ with_subst_mul_succ₂)
end WorldMultiplicationLevel03

namespace MyNat
  def succ_mul := WorldMultiplicationLevel03.succ_mul
end MyNat

namespace WorldMultiplicationLevel04
  open MyNat

  variable (a b : MyNat.Nat)

  theorem mul_comm : mul a b = mul b a :=
    b.recOn
      (zero :=
        /-
        goal: `mul a zero = mul zero a`

        this is easily derived from `zero = zero` with `mul_zero` and
        `zero_mul`.
        -/
        let refl_zero
          : zero = zero
          := rfl

        let subst_mul_zero
          : (zero = zero)
            = (mul a zero = zero)
          := congrArg
            (flip Eq $ zero)
            $ mul_zero a
        let with_subst_mul_zero
          : mul a zero = zero
          := Eq.mp subst_mul_zero refl_zero

        let subst_zero_mul
          : (mul a zero = zero)
            = (mul a zero = mul zero a)
          := congrArg
            (Eq $ mul a zero)
            $ Eq.symm $ zero_mul a
        Eq.mp subst_zero_mul with_subst_mul_zero
      )
      (λ n (ih : mul a n = mul n a) ↦
        /-
        goal: `mul a (succ n) = mul (succ n) a`

        `  mul a n         = mul n a         [by ih]`
        `→ add (mul a n) a = add (mul n a) a [by congrArg]`
        `→ mul a (succ n)  = add (mul n a) a [by mul_succ]`
        `→ mul a (succ n)  = mul (succ n) a  [by succ_mul]`
        -/

        let enadd
          : add (mul a n) a = add (mul n a) a
          := congrArg
            (flip add $ a)
            ih

        let subst_mul_succ
          : (add (mul a n) a = add (mul n a) a)
            = (mul a (succ n) = add (mul n a) a)
          := congrArg
            (flip Eq $ add (mul n a) a)
            $ mul_succ a n
        let with_subst_mul_succ
          : mul a (succ n) = add (mul n a) a
          := Eq.mp subst_mul_succ enadd

        let subst_succ_mul
          : (mul a (succ n) = add (mul n a) a)
            = (mul a (succ n) = mul (succ n) a)
          := congrArg
            (Eq $ mul a $ succ n)
            $ Eq.symm $ succ_mul n a
        Eq.mp subst_succ_mul with_subst_mul_succ)
end WorldMultiplicationLevel04

namespace MyNat
  def mul_comm := WorldMultiplicationLevel04.mul_comm
end MyNat

namespace WorldMultiplicationLevel05
  open MyNat

  variable (m : MyNat.Nat)

  theorem one_mul : mul one m = m :=
    -- start with `m = m`, then use `mul_one` and `add_comm`
    let refl_goal_right
      : m = m
      := rfl

    let subst_mul_one
      : (m = m)
        = (mul m one = m)
      := congrArg
        (flip Eq $ m)
        $ Eq.symm $ mul_one m
    let with_subst_mul_one
      : mul m one = m
      := Eq.mp subst_mul_one refl_goal_right

    let subst_mul_comm
      : (mul m one = m)
        = (mul one m = m)
      := congrArg
        (flip Eq $ m)
        $ mul_comm m one
    Eq.mp subst_mul_comm with_subst_mul_one
end WorldMultiplicationLevel05

namespace MyNat
  def one_mul := WorldMultiplicationLevel05.one_mul
end MyNat

namespace WorldMultiplicationLevel06
  open MyNat

  variable (m : MyNat.Nat)

  theorem two_mul : mul two m = add m m :=
    /-
    `  mul two m = mul two m         [by Eq.refl]`
    `→ mul two m = mul (succ one) m  [by two_eq_succ_one]`
    `→ mul two m = add (mul one m) m [by succ_mul]`
    `→ mul two m = add m m           [by one_mul]`
    -/

    let refl_goal_left
      : mul two m = mul two m
      := rfl

    let subst_two_eq_succ_one
      : (mul two m = mul two m)
        = (mul two m = mul (succ one) m)
      := congrArg
        (λ _a ↦
          mul two m = mul _a m)
        $ two_eq_succ_one
    let with_subst_two_eq_succ_one
      : mul two m = mul (succ one) m
      := Eq.mp subst_two_eq_succ_one refl_goal_left

    let subst_succ_mul
      : (mul two m = mul (succ one) m)
        = (mul two m = add (mul one m) m)
      := congrArg
        (Eq $ mul two m)
        $ succ_mul one m
    let with_subst_succ_mul
      : mul two m = add (mul one m) m
      := Eq.mp subst_succ_mul with_subst_two_eq_succ_one

    let subst_one_mul
      : (mul two m = add (mul one m) m)
        = (mul two m = add m m)
      := congrArg
        (λ _a ↦
          mul two m = add _a m)
        $ one_mul m
    Eq.mp subst_one_mul with_subst_succ_mul
end WorldMultiplicationLevel06

namespace MyNat
  def two_mul := WorldMultiplicationLevel06.two_mul
end MyNat

namespace WorldMultiplicationLevel07
  open MyNat

  variable (a b c : MyNat.Nat)

  theorem mul_add : mul a (add b c) = add (mul a b) (mul a c) :=
    c.recOn
      (zero :=
        /-
        goal: `mul a (add b zero) = add (mul a b) (mul a zero)`

        `  mul a b            = mul a b                    [by Eq.refl]`
        `→ mul a (add b zero) = mul a b                    [by add_zero]`
        `→ mul a (add b zero) = add (mul a b) zero         [by add_zero]`
        `→ mul a (add b zero) = add (mul a b) (mul a zero) [by mul_zero]`
        -/

        let refl_mul_a_b
          : mul a b = mul a b
          := rfl

        let subst_add_zero₁
          : (mul a b = mul a b)
            = (mul a (add b zero) = mul a b)
          := congrArg
            (λ _a ↦
              mul a _a = mul a b)
            $ add_zero b
        let with_subst_add_zero₁
          : mul a (add b zero) = mul a b
          := subst_add_zero₁.mp refl_mul_a_b

        let subst_add_zero₂
          : (mul a b = mul a b)
            = (mul a (add b zero) = add (mul a b) zero)
          := congrArg
            (Eq $ mul a $ add b zero)
            $ add_zero $ mul a b
        let with_subst_add_zero₂
          : mul a (add b zero) = add (mul a b) zero
          := subst_add_zero₂.mp with_subst_add_zero₁

        let subst_mul_zero
          : (mul a (add b zero) = add (mul a b) zero)
            = (mul a (add b zero) = add (mul a b) (mul a zero))
          := congrArg
            (λ _a ↦
              mul a (add b zero) = add (mul a b) _a)
            $ mul_zero a
        subst_mul_zero.mp with_subst_add_zero₂
      )
      (λ n (ih : mul a (add b n) = add (mul a b) (mul a n)) ↦
        /-
        goal: `mul a (add b (succ n)) = add (mul a b) (mul a (succ n))`

        by reasoning backwards, just like a tactic script, we can show a
        logical purrogression from `ih` to the goal:

        `  mul a (add b (succ n))  = add (mul a b) (mul a (succ n))`
        `← mul a (succ (add b n))  = add (mul a b) (mul a (succ n))   [by add_succ]`
        `← add (mul a (add b n)) a = add (mul a b) (mul a (succ n))   [by mul_succ]`
        `← add (mul a (add b n)) a = add (mul a b) (add (mul a n) a)  [by mul_succ]`
        `← add (mul a (add b n)) a = add (add (mul a b) (mul a n)) a  [by add_assoc]`
        `←      mul a (add b n)    =      add (mul a b) (mul a n)     [by congrArg]`
        -/
        let enadd
          : add (mul a (add b n)) a = add (add (mul a b) (mul a n)) a
          := congrArg
            (flip add $ a)
            ih

        let subst_add_assoc
          : (add (mul a (add b n)) a = add (add (mul a b) (mul a n)) a)
            = (add (mul a (add b n)) a = add (mul a b) (add (mul a n) a))
          := congrArg
            (Eq $ add (mul a (add b n)) a)
            $ add_assoc (mul a b) (mul a n) a
        let with_subst_add_assoc
          : add (mul a (add b n)) a = add (mul a b) (add (mul a n) a)
          := subst_add_assoc.mp enadd

        let subst_mul_succ₁
          : (add (mul a (add b n)) a = add (mul a b) (add (mul a n) a))
            = (add (mul a (add b n)) a = add (mul a b) (mul a (succ n)))
          := congrArg
            ((Eq $ add (mul a (add b n)) a) ∘ (add $ mul a b))
            $ mul_succ a n
        let with_subst_mul_succ₁
          : add (mul a (add b n)) a = add (mul a b) (mul a (succ n))
          := subst_mul_succ₁.mp with_subst_add_assoc

        let subst_mul_succ₂
          : (add (mul a (add b n)) a = add (mul a b) (mul a (succ n)))
            = (mul a (succ (add b n)) = add (mul a b) (mul a (succ n)))
          := congrArg
            (flip Eq $ add (mul a b) (mul a $ succ n))
            $ mul_succ a (add b n)
        let with_subst_mul_succ₂
          : mul a (succ (add b n)) = add (mul a b) (mul a (succ n))
          := subst_mul_succ₂.mp with_subst_mul_succ₁

        let subst_add_succ
          : (mul a (succ (add b n)) = add (mul a b) (mul a (succ n)))
            = (mul a (add b (succ n)) = add (mul a b) (mul a (succ n)))
          := congrArg
            ((flip Eq $ add (mul a b) (mul a $ succ n)) ∘ (mul a))
            $ add_succ b n
        subst_add_succ.mp with_subst_mul_succ₂
      )
end WorldMultiplicationLevel07

namespace MyNat
  def mul_add := WorldMultiplicationLevel07.mul_add
end MyNat

namespace WorldMultiplicationLevel08
  open MyNat

  variable (a b c : MyNat.Nat)

  theorem add_mul : mul (add a b) c = add (mul a c) (mul b c) :=
    /-
    by using `add_comm` and `mul_add`, we can avoid (explicit) induction

    `  mul (add a b) c         = add (mul a c) (mul b c)`
    `← mul c (add a b)         = add (mul a c) (mul b c) [by mul_comm]`
    `← add (mul c a) (mul c b) = add (mul a c) (mul b c) [by mul_add]`
    `← add (mul a c) (mul c b) = add (mul a c) (mul b c) [by mul_comm]`
    `← add (mul a c) (mul b c) = add (mul a c) (mul b c) [by mul_comm]`
    -/
    let refl_add_mulac_mulbc
      : add (mul a c) (mul b c) = add (mul a c) (mul b c)
      := rfl

    let subst_mul_comm₁
      : (add (mul a c) (mul b c) = add (mul a c) (mul b c))
        = (add (mul a c) (mul c b) = add (mul a c) (mul b c))
      := congrArg
        ((flip Eq $ add (mul a c) $ mul b c) ∘ (add $ mul a c))
        $ mul_comm b c
    let with_subst_mul_comm₁
      : add (mul a c) (mul c b) = add (mul a c) (mul b c)
      := subst_mul_comm₁.mp refl_add_mulac_mulbc

    let subst_mul_comm₂
      : (add (mul a c) (mul c b) = add (mul a c) (mul b c))
        = (add (mul c a) (mul c b) = add (mul a c) (mul b c))
      := congrArg
        ((flip Eq $ add (mul a c) $ mul b c) ∘ (flip add $ mul c b))
        $ mul_comm a c
    let with_subst_mul_comm₂
      : add (mul c a) (mul c b) = add (mul a c) (mul b c)
      := subst_mul_comm₂.mp with_subst_mul_comm₁

    let subst_mul_add
      : (add (mul c a) (mul c b) = add (mul a c) (mul b c))
        = (mul c (add a b) = add (mul a c) (mul b c))
      := congrArg
        (flip Eq $ add (mul a c) $ mul b c)
        $ Eq.symm $ mul_add c a b
    let with_subst_mul_add
      : mul c (add a b) = add (mul a c) (mul b c)
      := subst_mul_add.mp with_subst_mul_comm₂

    let subst_mul_comm₃
      : (mul c (add a b) = add (mul a c) (mul b c))
        = (mul (add a b) c = add (mul a c) (mul b c))
      := congrArg
        (flip Eq $ add (mul a c) $ mul b c)
        $ mul_comm c (add a b)
    subst_mul_comm₃.mp with_subst_mul_add
end WorldMultiplicationLevel08

namespace MyNat
  def add_mul := WorldMultiplicationLevel08.add_mul
end MyNat

namespace WorldMultiplicationLevel09
  open MyNat

  variable (a b c : MyNat.Nat)

  theorem mul_assoc : mul (mul a b) c = mul a (mul b c) :=
    c.recOn
      (zero :=
        -- goal: `mul (mul a b) zero = mul a (mul b zero)`; trivial
        rfl
      )
      (succ := λ n (ih : mul (mul a b) n = mul a (mul b n)) ↦
        /-
        goal: `mul (mul a b) (succ n) = mul a (mul b (succ n))`

        `  mul (mul a b) (succ n)                    = mul a (mul b (succ n))`
        `← mul (mul a b) (add n one)                 = mul a (mul b (succ n))            [by succ_eq_add_one]`
        `← mul (mul a b) (add n one)                 = mul a (mul b (add n one))         [by succ_eq_add_one]`
        `← add (mul (mul a b) n) (mul (mul a b) one) = mul a (mul b (add n one))         [by mul_add]`
        `← add (mul (mul a b) n) (mul a b)           = mul a (mul b (add n one))         [by mul_one]`
        `← add (mul (mul a b) n) (mul a b)           = mul a (add (mul b n) (mul b one)) [by mul_add]`
        `← add (mul (mul a b) n) (mul a b)           = mul a (add (mul b n) b)           [by mul_one]`
        `← add (mul (mul a b) n) (mul a b)           = add (mul a (mul b n)) (mul a b)   [by mul_add]`
        `←      mul (mul a b) n                      =      mul a (mul b n)              [by congrArg]`
        `←                                                                               [by ih]`
        -/
        let enadd
          : add (mul (mul a b) n) (mul a b) = add (mul a (mul b n)) (mul a b)
          := congrArg
            (flip add $ mul a b)
            ih

        let subst_mul_add₁
          : (add (mul (mul a b) n) (mul a b) = add (mul a (mul b n)) (mul a b))
            = (add (mul (mul a b) n) (mul a b) = mul a (add (mul b n) b))
          := congrArg
            (Eq $ add (mul (mul a b) n) $ mul a b)
            $ Eq.symm $ mul_add a (mul b n) b
        let with_subst_mul_add₁
          : add (mul (mul a b) n) (mul a b) = mul a (add (mul b n) b)
          := subst_mul_add₁.mp enadd

        let subst_mul_one₁
          : (add (mul (mul a b) n) (mul a b) = mul a (add (mul b n) b))
            = (add (mul (mul a b) n) (mul a b) = mul a (add (mul b n) (mul b one)))
          := congrArg
            ((Eq $ add (mul (mul a b) n) $ mul a b) ∘ (mul a ∘ (add $ mul b n)))
            $ Eq.symm $ mul_one b
        let with_subst_mul_one₁
          : add (mul (mul a b) n) (mul a b) = mul a (add (mul b n) (mul b one))
          := subst_mul_one₁.mp with_subst_mul_add₁

        let subst_mul_add₂
          : (add (mul (mul a b) n) (mul a b) = mul a (add (mul b n) (mul b one)))
            = (add (mul (mul a b) n) (mul a b) = mul a (mul b (add n one)))
          := congrArg
            ((Eq $ add (mul (mul a b) n) $ mul a b) ∘ mul a)
            $ Eq.symm $ mul_add b n one
        let with_subst_mul_add₂
          : add (mul (mul a b) n) (mul a b) = mul a (mul b (add n one))
          := subst_mul_add₂.mp with_subst_mul_one₁

        let subst_mul_one₂
          : (add (mul (mul a b) n) (mul a b) = mul a (mul b (add n one)))
            = (add (mul (mul a b) n) (mul (mul a b) one) = mul a (mul b (add n one)))
          := congrArg
            ((flip Eq $ mul a $ mul b $ add n one) ∘ (add $ mul (mul a b) n))
            $ Eq.symm $ mul_one $ mul a b
        let with_subst_mul_one₂
          : add (mul (mul a b) n) (mul (mul a b) one) = mul a (mul b (add n one))
          := subst_mul_one₂.mp with_subst_mul_add₂

        let subst_mul_add₃
          : (add (mul (mul a b) n) (mul (mul a b) one) = mul a (mul b (add n one)))
            = (mul (mul a b) (add n one) = mul a (mul b (add n one)))
          := congrArg
            (flip Eq $ mul a $ mul b $ add n one)
            $ Eq.symm $ mul_add (mul a b) n one
        let with_subst_mul_add₃
          : mul (mul a b) (add n one) = mul a (mul b (add n one))
          := subst_mul_add₃.mp with_subst_mul_one₂

        let subst_succ_eq_add_one₁
          : (mul (mul a b) (add n one) = mul a (mul b (add n one)))
           = (mul (mul a b) (succ n) = mul a (mul b (add n one)))
          := congrArg
            ((flip Eq $ mul a $ mul b $ add n one) ∘ (mul $ mul a b))
            $ succ_eq_add_one n
        let with_subst_succ_eq_add_one₁
          : mul (mul a b) (succ n) = mul a (mul b (add n one))
          := subst_succ_eq_add_one₁.mp with_subst_mul_add₃

        let subst_succ_eq_add_one₂
          : (mul (mul a b) (succ n) = mul a (mul b (add n one)))
            = (mul (mul a b) (succ n) = mul a (mul b (succ n)))
          := congrArg
            ((Eq $ mul (mul a b) $ succ n) ∘ mul a ∘ mul b)
            $ succ_eq_add_one n
        subst_succ_eq_add_one₂.mp with_subst_succ_eq_add_one₁
      )
end WorldMultiplicationLevel09

namespace MyNat
  def mul_assoc := WorldMultiplicationLevel09.mul_assoc
end MyNat
