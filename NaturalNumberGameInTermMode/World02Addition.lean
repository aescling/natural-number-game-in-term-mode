import NaturalNumberGameInTermMode.World01Tutorial

/-
Level 01 introduces the `induction` tactic

the calculus of inductive constructions *generates* the inductive urrinciple
fur an inductively defined type on definition of that type. ultimately, the
induction tactic reduces to using the automatically generated induction
purrinciple, as a form of purrimitve recursion.

this induction purrinciple is automatically generated:

`#check @MyNat.Nat.rec`
`@MyNat.Nat.rec :`
  `{motive : MyNat.Nat → Sort u_1} -- that which we wish to purrove; generally infurred`
    `→ motive MyNat.Nat.zero -- base case`
    `→ ((a : MyNat.Nat) → motive a → motive (MyNat.Nat.succ a)) --inductive case`
    `→ (t : MyNat.Nat) -- the MyNat.Nat being inducted on`
    `→ motive t`

`recOn` is also automatically generated, and swaps the function arguments
so as to make it more natural to structure a recursive proof:
`#check @MyNat.Nat.recOn`
`@MyNat.Nat.recOn :`
  `{motive : MyNat.Nat → Sort u_1} -- that which we wish to purrove; generally infurred`
    `→ (t : MyNat.Nat) -- the MyNat.Nat being inducted on`
    `→ motive MyNat.Nat.zero -- base case`
    `→ ((a : MyNat.Nat) → motive a → motive (MyNat.Nat.succ a)) -- inductive case`
    `→ motive t`

it should be noted that the code generator does not suppurrt the induction
purrinciples generated fur arbitrary inductive types; see
https://github.com/leanprover/lean4/issues/2049 and the issue linked therein.
the consequece of this limitation is that computable functions, such as
`MyNat.add` below, *cannot* be implemented using these recursors. this is a
limitation with Lean itself (one that i would regard as very serious!), and not
the underlying CIC.
-/

/-
we will prove this with `recOn`.

this purroof is a reimplementation of examples from the "Inductive Types"
chapter of _Theorem Proving In Lean_
-/
namespace WorldAdditionLevel01
  open MyNat

  theorem zero_add : ∀ n : MyNat.Nat, add zero n = n :=
    λ n ↦
      MyNat.Nat.recOn -- `(motive := λ x ↦ add zero x = x)`
        n -- `MyNat.Nat.recOn` can also be written `n.recOn`
        -- base case: `add zero zero = zero`
        rfl
        -- inductive case:
        $ λ m (ih : add zero m = m) ↦
          -- goal: `add zero (succ m) = succ m`

          let refl_goal_left
            : add zero (succ m) = add zero (succ m)
            := rfl

          -- use `succ_add` to derive `(add zero (succ m) = succ (add zero m))`
          let subst_add_succ
            : (add zero (succ m) = add zero (succ m))
              = (add zero (succ m) = succ (add zero m))
            := congrArg
              (λ _a ↦
                add zero (succ m) = _a)
              $ add_succ zero m
          let with_subst_add_succ
            : (add zero (succ m) = succ (add zero m))
            := Eq.mp subst_add_succ refl_goal_left

          -- use the inductive hypawthesis to derive the goal:
          let subst_ih
            : (add zero (succ m) = succ (add zero m))
              = (add zero (succ m) = succ m)
            := congrArg
              (λ _a ↦
                add zero (succ m) = succ _a)
              ih
          Eq.mp subst_ih with_subst_add_succ
end WorldAdditionLevel01

namespace MyNat
  def zero_add := WorldAdditionLevel01.zero_add
end MyNat

namespace WorldAdditionLevel02
  open MyNat

  variable (a b : MyNat.Nat)

  theorem succ_add : add (succ a) b = succ (add a b) :=
    b.recOn
      (zero :=
        /-
        goal: `add (succ a) zero = succ (add a zero)`

        we will derive the goal from `succ a = succ a` by repeated use of
        `add_zero`. this is more or less a translation of the tactical purroof
        i used in the game which rewrites the goal to something trivially
        proven by reflexivity.
        -/
        let refl_succ_a
          : succ a = succ a
          := rfl

        let subst_add_zero₁
          : (succ a = succ a)
            = (succ a = succ (add a zero))
          := congrArg
            (λ _a ↦
              succ a = succ _a)
            $ add_zero a
        let with_subst_add_zero₁
          : succ a = succ (add a zero)
          := Eq.mp subst_add_zero₁ refl_succ_a

        let subst_add_zero₂
          : (succ a = succ (add a zero))
            = (add (succ a) zero = succ (add a zero))
          := congrArg
            (Eq $ succ a) -- `a = b` is notation fur `Eq a b`
            $ add_zero $ succ a
        Eq.mp subst_add_zero₂ with_subst_add_zero₁)
      (succ := λ n (ih : add (succ a) n = succ (add a n)) ↦
        -- goal: `add (succ a) (succ n) = succ (add a (succ n))`

        -- like last time, we will work backwards
        let refl_succ_succ_add
          : succ (succ (add a n)) = succ (succ (add a n))
          := rfl

        -- we will have to substitute "backwards" here; hence `Eq.mpr`
        let subst_ih
          : (succ (add (succ a) n) = succ (succ (add a n)))
            = (succ (succ (add a n)) = succ (succ (add a n)))
          := congrArg
            (λ _a ↦
              succ _a = succ (succ (add a n)))
            ih
        let with_subst_ih
          : succ (add (succ a) n) = succ (succ (add a n))
          := Eq.mpr subst_ih refl_succ_succ_add

        -- repeated "backwards" substitutions of `add_succ` will derive the
        -- goal:
        let subst_add_succ₁
          : (add (succ a) (succ n) = succ (succ (add a n)))
            = (succ (add (succ a) n) = succ (succ (add a n)))
          := congrArg
            (λ _a ↦
              add (succ a) (succ n) = succ _a)
            $ add_succ a n
        let with_subst_add_succ₁
          : succ (add (succ a) n) = succ (succ (add a n))
          := Eq.mpr subst_add_succ₁ with_subst_ih

        let with_subst_add_succ₂
          : (add (succ a) (succ n) = succ (add a (succ n)))
            = (succ (add (succ a) n) = succ (add a (succ n)))
          := congrArg
            (λ _a ↦
              (add (succ a) (succ n)) = succ _a)
            $ add_succ a n
        -- fur some reason we need to coerce the types here
        let with_subst_add_succ₂
          : (add (succ a) (succ n) = succ (add a (succ n)))
          := Eq.mpr with_subst_add_succ₂ with_subst_add_succ₁
        with_subst_add_succ₂)
end WorldAdditionLevel02

namespace MyNat
  def succ_add := WorldAdditionLevel02.succ_add
end MyNat

-- this lemma proves helpful enough that i have extracted it out
def ensucc : ∀ {x y : MyNat.Nat}, x = y → MyNat.Nat.succ x = MyNat.Nat.succ y :=
  congrArg
    MyNat.Nat.succ

/-
from this point on i intend to stop translating my tactical purroofs from the
Game and resume writing terms from scratch. the results in these cases are not
really diffurentiable, the arguments being made in these term are basically
identical, if in reverse, to the ones made in my tactical purrofs. this is
arguably a good thing, if your goal is to understand the theory underlying a
tactical purroof.
-/
namespace WorldAdditionLevel03
  open MyNat

  variable (a b : MyNat.Nat)

  theorem add_comm : add a b = add b a :=
    b.recOn
      (zero :=
        /-
        goal: `add a zero = add zero a`

        this is obvious from `add_zero` and `zero_add`. we can work backwards
        from `a = a`
        -/
        let refl_a
          : a = a
          := rfl

        let subst_add_zero
          : (a = a)
            = (add a zero = a)
          := congrArg
              (Eq a)
              $ add_zero a
        let with_subst_add_zero
          : add a zero = a
          := Eq.mp subst_add_zero refl_a

        let subst_zero_add
          : (add a zero = a)
            = (add a zero = add zero a)
          := congrArg
              (Eq a)
              $ Eq.symm $ zero_add a
        Eq.mp subst_zero_add with_subst_add_zero)
      (succ := λ n (ih : add a n = add n a) ↦
        /-
        goal: `add a (succ n) = add (succ n) a`

        we will furst derive `succ (add a n) = succ (add n a)` from the
        inductive hypawthesis. the goal follows trivially from `add_succ` and
        `succ_add`.
        -/
        let with_ensucc
          : succ (add a n) = succ (add n a)
          := ensucc ih

        let subst_succ_add
          : (succ (add a n) = succ (add n a))
            = (succ (add a n) = add (succ n) a)
          := congrArg
            (Eq $ succ (add a n))
            $ Eq.symm $ succ_add n a
        let with_subst_succ_add
          : succ (add a n) = add (succ n) a
          := Eq.mp subst_succ_add with_ensucc

        let subst_add_succ
          : (succ (add a n) = add (succ n) a)
              = (add a (succ n) = add (succ n) a)
          := congrArg
            (flip Eq $ add (succ n) a)
            $ Eq.symm $ add_succ a n
        Eq.mp subst_add_succ with_subst_succ_add)
end WorldAdditionLevel03

namespace MyNat
  def add_comm := WorldAdditionLevel03.add_comm
end MyNat

namespace WorldAdditionLevel04
  open MyNat

  variable (a b c : MyNat.Nat)

  theorem add_assoc : add (add a b) c = add a (add b c) :=
    MyNat.Nat.recOn c
      (zero :=
        -- goal: `add (add a b) zero = add a (add b zero)`
        rfl)
      (succ := λ n (ih : add (add a b) n = add a (add b n)) ↦
        /-
        goal: `add (add a b) (succ n) = add a (add b (succ n))`

        we can reason why this should be true by repeated use of `add_succ`:

        `        add (add a b) n  =       add a (add b n)  [by ih]`
        `→ succ (add (add a b) n) = succ (add a (add b n)) [by ensucc]`
        `→ add (add a b) (succ n) = succ (add a (add b n)) [by Eq.symm add_succ]`
        `→ add (add a b) (succ n) = add a (succ (add b n)) [by Eq.symm add_succ]`
        `→ add (add a b) (succ n) = add a (add b (succ n)) [by Eq.symm add_succ]`

        our proof will follow the above argument exactly.
        -/

        let ensucc_ih
          : succ (add (add a b) n) = succ (add a (add b n))
          := ensucc ih

        let subst_add_succ₁
          : (succ (add (add a b) n) = succ (add a (add b n)))
            = (add (add a b) (succ n) = succ (add a (add b n)))
          := congrArg
            (flip Eq $ succ (add a (add b n)))
            $ Eq.symm $ add_succ (add a b) n
        let with_subst_add_succ₁
          : add (add a b) (succ n) = succ (add a (add b n))
          := Eq.mp subst_add_succ₁ ensucc_ih

        let subst_add_succ₂
          : (add (add a b) (succ n) = succ (add a (add b n)))
            = ((add (add a b) (succ n) = (add a (succ (add b n)))))
          := congrArg
            (Eq $ add (add a b) (succ n))
              $ Eq.symm $ add_succ a (add b n)
        let with_subst_add_succ₂
          : add (add a b) (succ n) = (add a (succ (add b n)))
          := Eq.mp subst_add_succ₂ with_subst_add_succ₁

        let subst_add_succ₃
          : (add (add a b) (succ n) = (add a (succ (add b n))))
            = (add (add a b) (succ n) = (add a (add b (succ n))))
          := congrArg
            (Eq $ add (add a b) (succ n))
              $ congrArg
                (add a)
                $ Eq.symm $ add_succ b n
        Eq.mp subst_add_succ₃ with_subst_add_succ₂)
end WorldAdditionLevel04

namespace MyNat
  def add_assoc := WorldAdditionLevel04.add_assoc
end MyNat

namespace WorldAdditionLevel05
  open MyNat

  variable (a b c : MyNat.Nat)

  theorem add_right_comm : add (add a b) c = add (add a c) b :=
    /-
    we will argue as follows:

    `  add (add a b) c = add (add a b) c [by Eq.refl]`
    `→ add (add a b) c = add a (add b c) [by add_assoc]`
    `→ add (add a b) c = add a (add c b) [by add_comm]`
    `→ add (add a b) c = add (add a c) b [by add_assoc]`
    -/

    let refl_goal_left
      : add (add a b) c = add (add a b) c
      := rfl

    let subst_add_assoc₁
      : (add (add a b) c = add (add a b) c)
        = (add (add a b) c = add a (add b c))
      :=
        congrArg
          (Eq $ add (add a b) c)
          $ add_assoc a b c
    let with_subst_add_assoc₁
      : add (add a b) c = add a (add b c)
      := Eq.mp subst_add_assoc₁ refl_goal_left

    let subst_add_comm
      : (add (add a b) c = add a (add b c))
        = (add (add a b) c = add a (add c b))
      := congrArg
        ((Eq $ add (add a b) c) ∘ add a)
        $ add_comm b c
    let with_subst_add_comm
      : add (add a b) c = add a (add c b)
      := Eq.mp subst_add_comm with_subst_add_assoc₁

    let with_subst_add_assoc₂
      : (add (add a b) c = add a (add c b))
        = (add (add a b) c = add (add a c) b)
      := congrArg
        (Eq $ add (add a b) c)
        $ Eq.symm $ add_assoc a c b
    Eq.mp with_subst_add_assoc₂ with_subst_add_comm
end WorldAdditionLevel05

namespace MyNat
  def add_right_comm := WorldAdditionLevel05.add_right_comm
end MyNat
