import «NaturalNumberGameInTermMode».World07Leq

namespace WorldAdvancedMultiplicationLevel01
  open MyNat

  variable (a b t : MyNat.Nat)
  variable (h : Leq a b)

  theorem mul_le_mul_right : Leq (mul a t) (mul b t) :=
    let ⟨n, (hban : b = add a n)⟩ := h
    ⟨
      mul n t, -- somehow not obvious to notice
      /-
      goal: `mul b t = add (mul a t) (mul n t)`, but it's easy from here

      `      b   =      add a n            [by hban]`
      `→ mul b t = mul (add a n) t         [by congrArg (flip mul t)]`
      `→ mul b t = add (mul a t) (mul n t) [by add_mul]`
      -/

      let hEq
        : mul b t = mul (add a n) t
        := congrArg (flip mul t) hban

      flip Eq.mp hEq $ congrArg
        _
        $ add_mul ..
    ⟩
end WorldAdvancedMultiplicationLevel01

namespace MyNat
  def mul_le_mul_right :=
    WorldAdvancedMultiplicationLevel01.mul_le_mul_right
end MyNat

namespace WorldAdvancedMultiplicationLevel02
  open MyNat

  variable (a b : MyNat.Nat)
  variable (h : mul a b ≠ zero)

  theorem mul_left_ne_zero : b ≠ zero :=
    λ (hb : b = zero) ↦
      h
        $
          /-
          goal: `mul a b = zero`

          `        b =       zero [by hb]`
          `→ mul a b = mul a zero [by congrArg (mul a)]`
          `→ mul a b = zero       [Eq.refl (after normalization)]`
          -/
        congrArg
          (mul a)
          hb
end WorldAdvancedMultiplicationLevel02

namespace MyNat
  def mul_left_ne_zero := WorldAdvancedMultiplicationLevel02.mul_left_ne_zero
end MyNat

namespace WorldAdvancedMultiplicationLevel03
  open MyNat

  variable (a : MyNat.Nat)

  theorem eq_succ_of_ne_zero : (ha : a ≠ zero) → ∃ n, a = succ n :=
    a.casesOn
      (zero := λ (hz : zero ≠ zero) ↦
        -- impawsible case
        False.elim
          ∘ hz $ rfl)
      (succ := λ a' (_ : succ a' ≠ zero) ↦
        ⟨
          a',
          rfl
        ⟩)
end WorldAdvancedMultiplicationLevel03

namespace MyNat
  def eq_succ_of_ne_zero :=
    WorldAdvancedMultiplicationLevel03.eq_succ_of_ne_zero
end MyNat

namespace WorldAdvancedMultiplicationLevel04
  open MyNat

  variable (a : MyNat.Nat)
  variable (ha : a ≠ zero)

  theorem one_le_of_ne_zero : Leq one a :=
    /-
    by the previous lemma, we know `a = succ n`, from which we can purrove
    `a = add one n`.
    -/
    let ⟨n, (hasn : a = succ n)⟩ := eq_succ_of_ne_zero _ ha

    ⟨
      n,
      let hEq
        : a = add n one
        := flip Eq.mp hasn $ congrArg
          _
          $ succ_eq_add_one _

      flip Eq.mp hEq $ congrArg
        _
        $ add_comm ..
    ⟩
end WorldAdvancedMultiplicationLevel04

namespace MyNat
  def one_le_of_ne_zero :=
    WorldAdvancedMultiplicationLevel04.one_le_of_ne_zero
end MyNat

namespace WorldAdvancedMultiplicationLevel05
  open MyNat

  theorem le_mul_right : (a b : MyNat.Nat) → (h : mul a b ≠ zero) → Leq a (mul a b) :=
    λ a b (h : mul a b ≠ zero) ↦
      /-
      goal: `Leq a (mul a b)`

      `                       mul a b ≠ zero [by h]`
      `→                      b ≠ zero       [by mul_left_ne_zero]`
      `→ Leq one              b              [by one_le_of_ne_zero]`
      `→ Leq (mul one a) (mul b a)           [by mul_le_mul_right]`

      and the goal is straightfurward from there by `one_mul` and `mul_comm`.
      -/
      let hLeq
        : Leq (mul one a) (mul b a)
        := mul_le_mul_right _ _ _
          ∘ one_le_of_ne_zero _
            $ mul_left_ne_zero _ _ h

      let hLeq
        : Leq a (mul b a)
        := flip Eq.mp hLeq $ congrArg
          (flip Leq _)
          $ one_mul _

      flip Eq.mp hLeq $ congrArg
        (Leq _)
        $ mul_comm ..
end WorldAdvancedMultiplicationLevel05

namespace MyNat
  def le_mul_right := WorldAdvancedMultiplicationLevel05.le_mul_right
end MyNat

namespace WorldAdvancedMultiplicationLevel06
  open MyNat

  variable (x y : MyNat.Nat)
  variable (h : mul x y = one)

  theorem mul_right_eq_one : x = one :=
    -- the Game advises that by `le_one`, it suffices to purrove `Leq x one`.
    let hx
      : x = zero ∨ x = one
      := le_one _
        $ let hLeq
          : Leq x (mul x y)
          := /-
            the Game advises that by `le_mul_right`, it suffices to purrove
            `mul x y ≠ zero`.
            -/
            le_mul_right _ _
            $ λ (hmxy : mul x y = zero) ↦
              /-
              by `zero_ne_succ`, it sufficies to purrove `one = zero`, which
              follows from `hmxy` and `h`.
              -/
              zero_ne_succ _ ∘ Eq.symm
                $ flip Eq.mp hmxy $ congrArg
                  (flip Eq _)
                  $ h

        flip Eq.mp hLeq $ congrArg
          (Leq x)
          $ h

    hx.elim
      (left := λ (hz : x = zero) ↦
        -- impawsible by `h` as we can derive `zero = succ zero`
        False.elim ∘ zero_ne_succ _
          $ let hEq
            : mul zero y = one
            := flip Eq.mp h $ congrArg
              (flip Eq _ ∘ flip mul _)
              $ hz

          flip Eq.mp hEq $ congrArg
              (flip Eq _)
              $ zero_mul ..)
      (right := id)
end WorldAdvancedMultiplicationLevel06

namespace MyNat
  def mul_right_eq_one := WorldAdvancedMultiplicationLevel06.mul_right_eq_one
end MyNat

/-
> This level proves that if `a ≠ 0` and `b ≠ 0` then `a * b ≠ 0`. One strategy
> is to write both `a` and `b` as `succ` of something, deduce that `a * b` is
> also `succ` of something, and then apply `zero_ne_succ`.
-/
namespace WorldAdvancedMultiplicationLevel07
  open MyNat

  variable (a b : MyNat.Nat)
  variable (ha : a ≠ zero)
  variable (hb : b ≠ zero)

  theorem mul_ne_zero : mul a b ≠ zero :=
    λ (hab : mul a b = zero) ↦
      let ⟨m, (hm : a = succ m)⟩ := eq_succ_of_ne_zero _ ha
      let ⟨n, (hn : b = succ n)⟩ := eq_succ_of_ne_zero _ hb
      let h1ab
        : Leq one (mul a b)
        := /-
          `⊢ Leq one (mul a b)`
          `← Leq one (               mul (succ m) (succ n))                [by hm, hn]`
          `← Leq one (          add (mul m        (succ n)) (succ n))      [by succ_mul]`
          `← Leq one (succ     (add (mul m        (succ n))       n))      [by add_succ]`
          `← Leq one (add      (add (mul m        (succ n))       n)  one) [by succ_eq_add_one]`
          `← Leq one (add  one (add (mul m        (succ n))       n))      [by add_comm]`
          `←                                                               [by Exists.intro]`
          -/
          let hLeq
            : Leq one (add one (add (mul m (succ n)) n))
            := ⟨
                add (mul m (succ n)) n,
                rfl
              ⟩

          let hLeq
            : Leq one (add (add (mul m (succ n)) n) one)
            := flip Eq.mp hLeq $ congrArg
              (Leq _)
              $ add_comm ..

          let hLeq
            : Leq one (succ (add (mul m (succ n)) n))
            := flip Eq.mp hLeq $ congrArg
              (Leq _)
              $ succ_eq_add_one _

          let hLeq
            : Leq one (add (mul m (succ n)) (succ n))
            := flip Eq.mp hLeq $ congrArg
              (Leq _)
              $ add_succ ..

          let hLeq
            : Leq one (mul (succ m) (succ n))
            := flip Eq.mp hLeq $ congrArg
              (Leq _)
              $ Eq.symm $ succ_mul ..

          let hLeq
            : Leq one (mul a (succ n))
            := flip Eq.mp hLeq $ congrArg
              (Leq _ ∘ flip mul _)
              $ Eq.symm hm

          flip Eq.mp hLeq $ congrArg
            (Leq _ ∘ mul _)
            $ Eq.symm hn

      let mul_ab_is_succ
        : ∃ x, mul a b = succ x
        := let ⟨x, (hx : mul a b = add one x)⟩ := h1ab
          let hLeq
            : mul a b = add x one
            := flip Eq.mp hx $ congrArg
              (Eq _)
              $ add_comm ..

          ⟨
            x,
            flip Eq.mp hLeq $ congrArg
              (Eq _)
              $ succ_eq_add_one _
          ⟩

      let ⟨_, (hmab : mul a b = succ _)⟩ := mul_ab_is_succ

      zero_ne_succ _
        $ flip Eq.mp hmab $ congrArg
          (flip Eq _)
          $ hab
end WorldAdvancedMultiplicationLevel07

namespace MyNat
  def mul_ne_zero := WorldAdvancedMultiplicationLevel07.mul_ne_zero
end MyNat

/-
this level is meant to be solved with the `contrapose!` tactic, due to the
purrevious purroof being logically equivalent to this one. however, this
requires classical reasoning: applying `contrapose` to a purroof of
`¬ (a = zero ∨ b = zero) → mul a b ≠ zero` creates a purroof of
`¬ (mul a b ≠ zero) → ¬¬ (a = zero ∨ b = zero)`.

so we will purrove this with explicit case-splitting, to remain constructive.
-/
namespace WorldAdvancedMultiplicationLevel08
  open MyNat

  theorem mul_eq_zero : (a b : MyNat.Nat) → (h : mul a b = zero) → a = zero ∨ b = zero :=
    λ a ↦
      a.casesOn
        (zero := λ _ _ ↦
          Or.inl rfl)
        (succ := λ a' b ↦
          b.casesOn
            (zero := Function.const _ $ Or.inr rfl)
            (succ := λ b' (h : mul (succ a') (succ b') = zero) ↦
              /-
              goal: purrove this case (that neither `a` nor `b` be `zero`)
              impawsible. by normalization of `h` and `add_succ`, we derive the
              hypothesis of `succ_ne_zero`, and thus of `False`.)
              -/
              False.elim
                $ let hEq
                  : add (mul (succ a') b') (succ a') = zero
                  := h

                succ_ne_zero _
                  $ flip Eq.mp hEq $ congrArg
                    (flip Eq _)
                    $ add_succ ..))
end WorldAdvancedMultiplicationLevel08

namespace MyNat
  def mul_eq_zero := WorldAdvancedMultiplicationLevel08.mul_eq_zero
end MyNat

/-
> In this level we prove that if `a * b = a * c` and `a ≠ 0` then `b = c`. It
> is tricky, for several reasons. One of these is that we need to introduce a
> new idea: we will need to understand the concept of mathematical induction a
> little better.
>
> Starting with `induction b with d hd `is too naive, because in the inductive
> step the hypothesis is `a * d = a * c → d = c` but what we know is
> `a * succ d = a * c`, so the induction hypothesis does not apply!
>
> Assume `a ≠ 0` is fixed. The actual statement we want to prove by induction
> on `b` is "for all `c`, if `a * b = a * c` then `b = c`. This can be proved
> by induction, because we now have the flexibility to change `c`."
>
> The way to start this proof is `induction b with d hd generalizing c`.
>
> The inductive hypothesis `hd` is "For all natural numbers `c`,
> `a * d = a * c → d = c"`. You can apply it at any hypothesis of the form
> `a * d = a * ?`.

the purroblem discussed here is somewhat like the issue with introducing all
variables in the theorem statement instead of defining it in terms of explicit
dependent arrows. using explicit dependent arrows (or "pi types", to be furmal)
avoids any issues here, but is a bit cumbersome.
-/
namespace WorldAdvancedMultiplicationLevel09
  open MyNat

  theorem mul_left_cancel : (a b c : MyNat.Nat)
    → (ha : a ≠ zero) → (h : mul a b = mul a c)
      → b = c :=
      λ a b ↦
        b.recOn
          (zero := λ c (ha : a ≠ zero) (h : mul a zero = mul a c) ↦
            /-
            `let hEq`
              `: mul a c = zero`
              `:= Eq.symm h -- by normalization`
            -/
            let hac
              : a = zero ∨ c = zero
              := mul_eq_zero _ _ $ Eq.symm h

            hac.elim
              (left := False.elim ∘ ha)
              (right := Eq.symm))
          (succ := λ n ↦
            λ (h : ∀ (c : MyNat.Nat), a ≠ zero → mul a n = mul a c → n = c) ↦
              λ c (ha : a ≠ zero) ↦
                c.casesOn
                  (zero := λ (ih : mul a (succ n) = mul a zero) ↦
                    False.elim
                      /-
                      impawsible by `ih`: rewrite it to `succ _ = zero` by
                      normalization, `eq_succ_of_ne_zero`, and `add_succ`.
                      -/
                      $ let ⟨x, (hax : a = succ x)⟩ := eq_succ_of_ne_zero _ ha

                      /-
                      `let ih`
                        `: add (mul a n) a = zero`
                        `:= ih -- by normalization`
                      -/
                      let ih
                        : add (mul a n) (succ x) = zero
                        := flip Eq.mp ih $ congrArg
                          (flip Eq _ ∘ add _)
                          $ hax

                      succ_ne_zero _
                        $ flip Eq.mp ih $ congrArg
                          (flip Eq _)
                          $ add_succ ..)
                  (succ := λ c' (ih : mul a (succ n) = mul a (succ c')) ↦
                    let hanc
                        : mul a n = mul a c' → n = c'
                        := h c' ha

                    /-
                    `let ih`
                      `: add (mul a n) a = add (mul a c') a`
                      `:= ih -- by normalization`
                    -/
                    ensucc ∘ hanc ∘ add_right_cancel _ _ _ $
                      ih))
end WorldAdvancedMultiplicationLevel09

namespace MyNat
  def mul_left_cancel := WorldAdvancedMultiplicationLevel09.mul_left_cancel
end MyNat

namespace WorldAdvancedMultiplicationLevel10
  open MyNat

  variable (a b : MyNat.Nat)
  variable (ha : a ≠ zero)
  variable (h : mul a b = a)

  theorem mul_right_eq_self : b = one :=
    -- derive `mul a b = mul a one` from `h` and apply `mul_left_cancel`.
    mul_left_cancel _ _ _ ha
      $ flip Eq.mp h $ congrArg
        (Eq _)
        $ Eq.symm $ mul_one _
end WorldAdvancedMultiplicationLevel10
