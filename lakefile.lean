import Lake
open Lake DSL

package «natural-number-game-in-term-mode» where
  weakLeanArgs := #[
    "-DwarningAsError=true"
  ]

@[default_target]
lean_lib «NaturalNumberGameInTermMode» where
  -- add library configuration options here
