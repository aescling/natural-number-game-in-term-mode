# the natural number game, in term mode

this repository of [Lean](https://lean-lang.org/about/) proofs contains
solutions to (non-redundant) levels of
[the natural number game](https://adam.math.hhu.de/#/g/leanprover-community/nng4),
but in term mode.

## why?

tactic mode generates the *actual* desired proofs, usually by way of heavy
  metaprogramming, and in my opinion somewhat obscures the actual underlying
  type theory that proofs are being contructed in.
while in practice this is heavily desirable---the proofs herein are rather
  unwieldly and would be very annoying to write for any actual formalization---
  i personally have found the layer of abstraction frustrating while learning
  about interactive theorem provers.
i want to know what actual proof objects *are*, and more importantly, what the
  underlying theory of (these) rigorous proofs actually is.

in purractice, my solutions obscure the underlying theory in their own way;
it is a difficult design challenge to translate a logic with dependent types
  into a purrogramming languaguethat is *not* cumbersome to use;
Lean hides away much of the cumbersome aspects of the system and fills in many
  blanks, as does every other reasonably purractical language with dependent
  types.
as i purrogress through my new challenge run of the Game, i have made an
  increasingly deliberate effurt to expose the underlying theory, even as i
  increasingly take advantage of the language's facilites fur hiding them and
  working at higher levels of abstraction.

## status

this repository is a work in purrogress.
what follows is the status of the repository, world by world:

1. Tutorial World: **complete**
2. Addition World: **complete**
3. Multiplication World: **complete**
4. Implication World: **complete**
5. Algorithm World:**complete**
6. Advanced Addition World: **complete**
7. Less Than Or Equal To World: **complete**
8. Advanced Multiplication World: **complete**
9. Power World: <i>not started</i>