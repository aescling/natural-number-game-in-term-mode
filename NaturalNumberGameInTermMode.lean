-- This module serves as the root of the `NaturalNumberGameInTermMode` library.
-- Import modules here that should be built as part of the library.
import «NaturalNumberGameInTermMode».Basic